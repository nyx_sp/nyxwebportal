﻿using System;
using System.Threading.Tasks;
using MimeKit;
using MailKit.Net.Smtp;
using MailKit.Security;
using System.Text;
using System.Net.Mail;

namespace NyxWebPortal.Services
{
    // This class is used by the application to send Email and SMS
    // when you turn on two-factor authentication in ASP.NET Identity.
    // For more details see this link https://go.microsoft.com/fwlink/?LinkID=532713
    public class MessageServices : IEmailSender, ISmsSender
    {
        private static string mailPassword;
        
        public static void SetMailPassword(string password)
        {
            mailPassword = password;
        }
        
        public Task SendEmailAsync(string email, string subject, string message)
        {
            // Plug in your email service here to send an email.
            var emailMessage = new MimeMessage();
            emailMessage.From.Add(new MailboxAddress("Nyx Deployment", "nyxdeployment@nyx.io"));
            emailMessage.To.Add(new MailboxAddress("", email));
            emailMessage.Subject = subject;
            BodyBuilder bodyBuilder = new BodyBuilder();
            bodyBuilder.HtmlBody = message;
            emailMessage.Body = bodyBuilder.ToMessageBody();
            

            using (var client = new MailKit.Net.Smtp.SmtpClient())
            {
                client.ServerCertificateValidationCallback = (s, certificate, chain, sslPolicyErrors) => true;
                client.Connect("smtp.mailgun.org", 587);
                // Note: since we don't have an OAuth2 token, disable
                // the XOAUTH2 authentication mechanism.
                client.AuthenticationMechanisms.Remove("XOAUTH2");
                // Note: only needed if the SMTP server requires authentication
                client.Authenticate("postmaster@mail.nyxintelligence.com", mailPassword);
                client.Send(emailMessage);
                client.Disconnect(true);
            }
            return Task.FromResult(0);
        }

        public Task SendCalenderInviteEmailAsync(string email, string subject, string message, DateTimeOffset startDate, DateTimeOffset endDate, string organiser, string location)
        {
            // Plug in your email service here to send an email.
            var emailMessage = new MimeMessage();
            emailMessage.From.Add(new MailboxAddress("Nyx Deployment", "nyxdeployment@nyx.io"));
            emailMessage.To.Add(new MailboxAddress("", email));
            emailMessage.Subject = subject;
            BodyBuilder bodyBuilder = new BodyBuilder();
            bodyBuilder.HtmlBody = message;

            StringBuilder str = new StringBuilder();
            str.AppendLine("BEGIN:VCALENDAR");
            str.AppendLine("VERSION:2.0");
            str.AppendLine(string.Format("PRODID:{0}", organiser));
            str.AppendLine("X-WR-CALNAME:"+subject);
            str.AppendLine("CALSCALE:GREGORIAN");

            str.AppendLine("BEGIN:VTIMEZONE");
            str.AppendLine("TZID:Asia/Shanghai");
            str.AppendLine("TZURL:http://tzurl.org/zoneinfo-outlook/Asia/Shanghai");
            str.AppendLine("X-LIC-LOCATION:Asia/Shanghai");

            str.AppendLine("BEGIN:STANDARD");
            str.AppendLine("TZOFFSETFROM:+0800");
            str.AppendLine("TZOFFSETTO:+0800");
            str.AppendLine("TZNAME:CST");
            str.AppendLine(string.Format("DTSTART:{0:yyyyMMddTHHmmssZ}", startDate.ToUniversalTime().ToString("yyyyMMdd\\THHmmss\\Z")));
            str.AppendLine("END:STANDARD");

            str.AppendLine("END:VTIMEZONE");
            //str.AppendLine("METHOD:REQUEST");
            str.AppendLine("BEGIN:VEVENT");
            str.AppendLine(string.Format("DTSTAMP:{0:yyyyMMddTHHmmssZ}", DateTime.Now.ToUniversalTime().ToString("yyyyMMdd\\THHmmss\\Z")));
            str.AppendLine("UID:"+Guid.NewGuid().ToString());
            str.AppendLine(string.Format("DTSTART:{0:yyyyMMddTHHmmssZ}", startDate.ToUniversalTime().ToString("yyyyMMdd\\THHmmss\\Z")));
            str.AppendLine(string.Format("DTEND:{0:yyyyMMddTHHmmssZ}", endDate.ToUniversalTime().ToString("yyyyMMdd\\THHmmss\\Z")));

            //str.AppendLine("LOCATION:" + location);
            str.AppendLine(string.Format("SUMMARY:{0}", subject));
            //str.AppendLine(string.Format("ORGANIZER:MAILTO:{0}", organiser));

            str.AppendLine("END:VEVENT");
            str.AppendLine("END:VCALENDAR");
            System.Net.Mime.ContentType ct = new System.Net.Mime.ContentType("text/calendar");
            ct.Parameters.Add("method", "REQUEST");
            AlternateView avCal = AlternateView.CreateAlternateViewFromString(str.ToString(), ct);
            bodyBuilder.Attachments.Add("Invite.ics",avCal.ContentStream);
            emailMessage.Body = bodyBuilder.ToMessageBody();

            using (var client = new MailKit.Net.Smtp.SmtpClient())
            {
                client.ServerCertificateValidationCallback = (s, certificate, chain, sslPolicyErrors) => true;
                client.Connect("smtp.mailgun.org", 587);
                // Note: since we don't have an OAuth2 token, disable
                // the XOAUTH2 authentication mechanism.
                client.AuthenticationMechanisms.Remove("XOAUTH2");
                // Note: only needed if the SMTP server requires authentication
                client.Authenticate("postmaster@mail.nyxintelligence.com", mailPassword);
                client.Send(emailMessage);
                client.Disconnect(true);
            }
            return Task.FromResult(0);
        }

        public Task SendSmsAsync(string number, string message)
        {
            // Plug in your SMS service here to send a text message.
            return Task.FromResult(0);
        }
    }
}
