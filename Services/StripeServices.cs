﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace NyxWebPortal.Services
{
    public class StripeServices
    {
        public static async Task<string> StripeAccessToken(string code)
        {
            string apiUrl = "https://connect.stripe.com/oauth/token";

            HttpClient httpClient = new HttpClient();
            var values = new Dictionary<string, string>
            {
              {"client_secret", Environment.GetEnvironmentVariable("StripeApiKey")},
              {"code", code},
              {"grant_type", "authorization_code"}
            };
            var content = new FormUrlEncodedContent(values);
            HttpResponseMessage response = await httpClient.PostAsync(apiUrl, content);
            return await response.Content.ReadAsStringAsync();

        }

        public static async Task<string> CreateCustomer(string code, string email)
        {
            string apiUrl = "https://api.stripe.com/v1/customers";

            HttpClient httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Environment.GetEnvironmentVariable("StripeApiKey"));
            //httpClient.DefaultRequestHeaders.Add("Stripe-Account", code);
            var values = new Dictionary<string, string>
            {
              {"source", code},
              {"email",email}
            };
            var content = new FormUrlEncodedContent(values);
            HttpResponseMessage response = await httpClient.PostAsync(apiUrl, content);
            return await response.Content.ReadAsStringAsync();
        }

        public static async Task<string> Subscribe(string StripeCustomerId, string StripePlanId)
        {
            string apiUrl = "https://api.stripe.com/v1/subscriptions";

            HttpClient httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Environment.GetEnvironmentVariable("StripeApiKey"));
            //httpClient.DefaultRequestHeaders.Add("Stripe-Account", code);
            var values = new Dictionary<string, string>
            {
                {"customer", StripeCustomerId},
                {"items[0][plan]", StripePlanId},
                {"items[0][quantity]","1"}
            };
            var content = new FormUrlEncodedContent(values);
            HttpResponseMessage response = await httpClient.PostAsync(apiUrl, content);
            return await response.Content.ReadAsStringAsync();
        }
    
        public static async Task<string> ChangeSubscription(string StripeSubscriptionId, string NewStripePlanId)
        {
            string apiUrl = "https://api.stripe.com/v1/subscriptions/"+ StripeSubscriptionId;
            
            HttpClient httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Environment.GetEnvironmentVariable("StripeApiKey"));
            HttpResponseMessage getRsesponse = await httpClient.GetAsync(apiUrl);

            string subIdFromStripe = "";

            var result = await getRsesponse.Content.ReadAsStringAsync();
            JObject jObject = JObject.Parse(result);
            var ErrorCheck = jObject["error"];
            if (ErrorCheck == null)
            {
                subIdFromStripe = jObject["items"]["data"][0]["id"].ToString();
            }
            else
            {
                return "Error";

            }

            httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Environment.GetEnvironmentVariable("StripeApiKey"));
            var values = new Dictionary<string, string>
            {
                {"cancel_at_period_end", "false" },
                {"items[0][id]", subIdFromStripe},
                {"items[0][plan]",NewStripePlanId}
            };
            var content = new FormUrlEncodedContent(values);
            HttpResponseMessage response = await httpClient.PostAsync(apiUrl, content);
            return await response.Content.ReadAsStringAsync();
        }

        public static async Task<string> GetPlanIdOnSubId(string StripeSubscriptionId)
        {
            string apiUrl = "https://api.stripe.com/v1/subscriptions/" + StripeSubscriptionId;

            HttpClient httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Environment.GetEnvironmentVariable("StripeApiKey"));
            HttpResponseMessage getRsesponse = await httpClient.GetAsync(apiUrl);

            var result = await getRsesponse.Content.ReadAsStringAsync();
            JObject jObject = JObject.Parse(result);
            var ErrorCheck = jObject["error"];
            if (ErrorCheck == null)
            {
                return jObject["items"]["data"][0]["plan"]["id"].ToString();
            }
            else
            {
                return "Error";

            }
        }


        public static async Task<string> StripePaymentTest(string code, string card) {
            string apiUrl = "https://api.stripe.com/v1/charges";

            HttpClient httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Environment.GetEnvironmentVariable("StripeApiKey"));
            httpClient.DefaultRequestHeaders.Add("Stripe-Account", code);
            var values = new Dictionary<string, string>
            {
              {"currency", "SGD"},
              {"amount", "1000"},
              {"description", "test"},
              //Obtain with stripe js
              {"source",card},
              {"application_fee", "123"}
            };
            var content = new FormUrlEncodedContent(values);
            HttpResponseMessage response = await httpClient.PostAsync(apiUrl, content);
            return await response.Content.ReadAsStringAsync();

        }

        public static async Task<string> StripePayment(string code, string card, int amount, string desc)
        {
            string apiUrl = "https://api.stripe.com/v1/charges";

            HttpClient httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Environment.GetEnvironmentVariable("StripeApiKey"));
            httpClient.DefaultRequestHeaders.Add("Stripe-Account", code);
            var values = new Dictionary<string, string>
            {
              {"currency", "SGD"},
              {"amount", amount.ToString()},
              {"description", desc},
              //Obtain with stripe js
              {"source",card},
              {"application_fee", "50"}
            };
            var content = new FormUrlEncodedContent(values);
            HttpResponseMessage response = await httpClient.PostAsync(apiUrl, content);
            return await response.Content.ReadAsStringAsync();
        }
    }
}
