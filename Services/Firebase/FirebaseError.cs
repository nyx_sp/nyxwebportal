﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace NyxWebPortal.Services.Firebase
{
    public class FirebaseError
    {
        [JsonProperty("code")]
        public int Code { get; set; }
        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("error")]
        public FirebaseErrorCollection ErrorCollection { get; set; }
    }
    
    public class FirebaseErrorCollection
    {
        [JsonProperty("errors")]
        public IList<FirebaseErrorItem> Errors { get; set; }
    }

    public class FirebaseErrorItem
    {
        [JsonProperty("domain")]
        public string domain { get; set; }
        [JsonProperty("reason")]
        public string Reason { get; set; }
        [JsonProperty("message")]
        public string Message { get; set; }
    }
    
    public class FirebaseServiceException : Exception
    {
        public FirebaseError Error { get; set; }
        
        public FirebaseServiceException(string message, FirebaseError error) : base(message)
        {
            Error = error;
        }
    }
}