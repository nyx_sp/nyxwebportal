﻿using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using GAuth = Google.Apis.Auth.OAuth2;
using NyxWebPortal.Utils;
using NyxDatabase.Models;
using System;
using System.Collections.Generic;
using System.IO;
using Org.BouncyCastle.Crypto.Parameters;
using Jose;

namespace NyxWebPortal.Services.Firebase
{
    public class FirebaseService
    {
        private const string _firebaseUrl = "https://nyxprod040718.firebaseio.com/";
        private const string _verifyCustomTokenUrl = "https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyCustomToken";
        private const string _idTokenUrl = "https://securetoken.googleapis.com/v1/token";
        private const string _emailSignUpUrl = "https://www.googleapis.com/identitytoolkit/v3/relyingparty/signupNewUser";
        private const string _emailSignInUrl = "https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword";
        private const string _issuerURL = "https://identitytoolkit.googleapis.com/google.identity.identitytoolkit.v1.IdentityToolkit";

        private static RsaPrivateCrtKeyParameters _rsaParams;
        private static object _rsaParamsLocker = new object();

        private string _webApiKey;
        private readonly ServiceAccountCredential _sac;
        private HttpClient _client;

        public FirebaseService(string webApiKey, ServiceAccountCredential sac)
        {
            _webApiKey = webApiKey;
            _sac = sac;
            _client = new HttpClient();
        }

        public async Task<VerifyCustomTokenResponse> VerifyCustomTokenAsync(string token, bool returnSecureToken = true)
        {
            JObject json = new JObject();
            json["token"] = token;
            json["returnSecureToken"] = returnSecureToken;
            StringContent payload = new StringContent(json.ToString(Formatting.None), Encoding.UTF8, "application/json");

            HttpResponseMessage res = await _client.PostAsync($"{_verifyCustomTokenUrl}?key={_webApiKey}", payload);
            VerifyCustomTokenResponse firebaseRes = JsonConvert.DeserializeObject<VerifyCustomTokenResponse>(await res.Content.ReadAsStringAsync());

            if (firebaseRes.ErrorCollection != null && firebaseRes.ErrorCollection.Errors.Count > 0)
            {
                throw new FirebaseServiceException("An error occured in when requesting from firebase", firebaseRes);
            }

            return firebaseRes;
        }

        public async Task<IdTokenResponse> IdTokenAsync(string refreshToken)
        {
            JObject json = new JObject();
            json["grant_type"] = "refresh_token";
            json["refresh_token"] = refreshToken;
            StringContent payload = new StringContent(json.ToString(Formatting.None), Encoding.UTF8, "application/json");

            HttpResponseMessage res = await _client.PostAsync($"{_idTokenUrl}?key={_webApiKey}", payload);
            IdTokenResponse firebaseRes = JsonConvert.DeserializeObject<IdTokenResponse>(await res.Content.ReadAsStringAsync());

            if (firebaseRes.ErrorCollection != null && firebaseRes.ErrorCollection.Errors.Count > 0)
            {
                throw new FirebaseServiceException("An error occured in when requesting from firebase", firebaseRes);
            }

            return firebaseRes;
        }
        
        // get the code from this like : https://stackoverflow.com/questions/38188122/firebase-3-creating-a-custom-authentication-token-using-net-and-c-sharp
        public async Task<string> CustomTokenAsync(string refreshToken)
        {
            IdTokenResponse fRes = await this.IdTokenAsync(refreshToken);

            // Get the RsaPrivateCrtKeyParameters if we haven't already determined them
            if (_rsaParams == null)
            {
                lock (_rsaParamsLocker)
                {
                    if (_rsaParams == null)
                    {
                        StreamReader sr = new StreamReader(GenerateStreamFromString(_sac.PrivateKey.Replace(@"\n", "\n")));
                        var pr = new Org.BouncyCastle.OpenSsl.PemReader(sr);
                        _rsaParams = (RsaPrivateCrtKeyParameters)pr.ReadObject();
                    }
                }
            }

            var payload = new Dictionary<string, object> {
                {"uid", fRes.UserId},
                {"iat", secondsSinceEpoch(DateTime.UtcNow)},
                {"exp", secondsSinceEpoch(DateTime.UtcNow.AddHours(1))},
                {"aud", _issuerURL},
                {"iss", _sac.ClientEmail},
                {"sub", _sac.ClientEmail},
            };

            return JWT.Encode(payload, Org.BouncyCastle.Security.DotNetUtilities.ToRSA(_rsaParams), JwsAlgorithm.RS256);
        }

        private static long secondsSinceEpoch(DateTime dt)
        {
            TimeSpan t = dt - new DateTime(1970, 1, 1);
            return (long)t.TotalSeconds;
        }

        private static Stream GenerateStreamFromString(string s)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }


        public async Task<EmailSignUpResponse> EmailSignUpAsync(string email, string password, bool returnSecureToken = true)
        {
            JObject json = new JObject();
            json["email"] = email;
            json["password"] = password;
            json["returnSecureToken"] = returnSecureToken;
            StringContent payload = new StringContent(json.ToString(Formatting.None), Encoding.UTF8, "application/json");

            HttpResponseMessage res = await _client.PostAsync($"{_emailSignUpUrl}?key={_webApiKey}", payload);
            EmailSignUpResponse firebaseRes = JsonConvert.DeserializeObject<EmailSignUpResponse>(await res.Content.ReadAsStringAsync());

            if (firebaseRes.ErrorCollection != null && firebaseRes.ErrorCollection.Errors.Count > 0)
            {
                throw new FirebaseServiceException($"An error occured in when requesting from firebase: {firebaseRes.Code} - {firebaseRes.Message}", firebaseRes);
            }

            return firebaseRes;
        }

        public async Task<EmailSignInResponse> EmailSignInAsync(string email, string password, bool returnSecureToken = true)
        {
            JObject json = new JObject();
            json["email"] = email;
            json["password"] = password;
            json["returnSecureToken"] = returnSecureToken;
            StringContent payload = new StringContent(json.ToString(Formatting.None), Encoding.UTF8, "application/json");

            HttpResponseMessage res = await _client.PostAsync($"{_emailSignUpUrl}?key={_webApiKey}", payload);
            EmailSignInResponse firebaseRes = JsonConvert.DeserializeObject<EmailSignInResponse>(await res.Content.ReadAsStringAsync());

            if (firebaseRes.ErrorCollection != null && firebaseRes.ErrorCollection.Errors.Count > 0)
            {
                throw new FirebaseServiceException("An error occured in when requesting from firebase", firebaseRes);
            }

            return firebaseRes;
        }

        public async Task<string> GetAdminAccessTokenAsync()
        {
            GAuth.ServiceAccountCredential gsac = new GAuth.ServiceAccountCredential(
                new GAuth.ServiceAccountCredential.Initializer(_sac.ClientEmail)
                {
                    Scopes = new[]
                    {
                        "https://www.googleapis.com/auth/userinfo.email",
                        "https://www.googleapis.com/auth/firebase.database"
                    }
                }.FromPrivateKey(_sac.PrivateKey));

            return await gsac.GetAccessTokenForRequestAsync();
        }

        public async Task CreateBotAsync(CompanyAdmin companyAdmin, Bot bot)
        {
            string token = await GetAdminAccessTokenAsync();

            IdTokenResponse res = await IdTokenAsync(companyAdmin._FirebaseRefreshToken);

            JObject whitelistJson = new JObject();
            whitelistJson[bot.Id] = new JObject();
            whitelistJson[bot.Id][res.UserId] = true;
            StringContent whitelistPayload = new StringContent(whitelistJson.ToString(Formatting.None), Encoding.UTF8, "application/json");

            HttpResponseMessage whitelistRes = await _client.PatchAsync($"{_firebaseUrl}Whitelist.json?access_token={token}", whitelistPayload);

            switch (bot.Type)
            {
                case "GRC":
                    // no intents for GRC
                    JObject botJson = new JObject();
                    botJson[bot.Id] = new JObject();
                    botJson[bot.Id]["Intents"] = false;
                    StringContent botPayload = new StringContent(botJson.ToString(Formatting.None), Encoding.UTF8, "application/json");
                    HttpResponseMessage botRes = await _client.PatchAsync($"{_firebaseUrl}Bot.json?access_token={token}", botPayload);
                    break;
            }
        }

        public async Task SetWhiteListAsync(NyxCustomer user, string botId, bool allow)
        {
            string token = await GetAdminAccessTokenAsync();

            IdTokenResponse res = await IdTokenAsync(user._FirebaseRefreshToken);

            JObject whitelistJson = new JObject();
            whitelistJson[res.UserId] = allow;
            StringContent whitelistPayload = new StringContent(whitelistJson.ToString(Formatting.None), Encoding.UTF8, "application/json");

            HttpResponseMessage whitelistRes = await _client.PatchAsync($"{_firebaseUrl}Whitelist/{botId}.json?access_token={token}", whitelistPayload);
        }
    }
}
