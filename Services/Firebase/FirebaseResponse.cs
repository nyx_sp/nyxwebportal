﻿using System;
using Newtonsoft.Json;

namespace NyxWebPortal.Services.Firebase
{
    public class VerifyCustomTokenResponse : FirebaseError
    {
        [JsonProperty("kind")]
        public string Kind { get; set; }
        [JsonProperty("idToken")]
        public string IdToken { get; set; }
        [JsonProperty("refreshToken")]
        public string RefreshToken { get; set; }
        [JsonProperty("expiresIn")]
        public string ExpiresIn { get; set; }
    }
    
    public class IdTokenResponse : FirebaseError
    {
        [JsonProperty("expires_in")]
        public string ExpiresIn { get; set; }
        [JsonProperty("token_type")]
        public string TokenType { get; set; }
        [JsonProperty("refresh_token")]
        public string RefreshToken { get; set; }
        [JsonProperty("id_token")]
        public string IdToken { get; set; }
        [JsonProperty("user_id")]
        public string UserId { get; set; }
        [JsonProperty("project_id")]
        public string ProjectId { get; set; }
    }
    
    public class EmailSignUpResponse : FirebaseError
    {
        [JsonProperty("kind")]
        public string Kind { get; set; }
        [JsonProperty("idToken")]
        public string IdToken { get; set; }
        [JsonProperty("email")]
        public string Email { get; set; }
        [JsonProperty("refreshToken")]
        public string RefreshToken { get; set; }
        [JsonProperty("expiresIn")]
        public string ExpiresIn { get; set; }
        [JsonProperty("localId")]
        public string LocalId { get; set; }
    }
    
    public class EmailSignInResponse : FirebaseError
    {
        
        [JsonProperty("kind")]
        public string Kind { get; set; }
        [JsonProperty("idToken")]
        public string IdToken { get; set; }
        [JsonProperty("email")]
        public string Email { get; set; }
        [JsonProperty("refreshToken")]
        public string RefreshToken { get; set; }
        [JsonProperty("expiresIn")]
        public string ExpiresIn { get; set; }
        [JsonProperty("localId")]
        public string LocalId { get; set; }
        [JsonProperty("registered")]
        public string Registered { get; set; }
    }
}
