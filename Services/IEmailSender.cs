﻿using System;
using System.Threading.Tasks;

namespace NyxWebPortal.Services
{
    public interface IEmailSender
    {
        Task SendEmailAsync(string email, string subject, string message);

        Task SendCalenderInviteEmailAsync(string email, string subject, string message, DateTimeOffset startDate, DateTimeOffset endDate, string organiser, string location);
    }
}
