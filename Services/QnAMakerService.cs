﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NyxWebPortal.Services
{
    public class QnAMakerService
    {
        private readonly string host = "https://westus.api.cognitive.microsoft.com/qnamaker/v4.0";
        private readonly string key = "1702a0f89ef24b4980849f1c43432aeb";

        public struct Response
        {
            public HttpResponseHeaders headers;
            public string response;

            public Response(HttpResponseHeaders headers, string response)
            {
                this.headers = headers;
                this.response = response;
            }
        }
        private async Task<Response> Post(string uri, string body)
        {
            using (var client = new HttpClient())
            using (var request = new HttpRequestMessage())
            {
                request.Method = HttpMethod.Post;
                request.RequestUri = new Uri(uri);
                request.Content = new StringContent(body, Encoding.UTF8, "application/json");
                request.Headers.Add("Ocp-Apim-Subscription-Key", key);

                var response = await client.SendAsync(request);
                var responseBody = await response.Content.ReadAsStringAsync();
                return new Response(response.Headers, responseBody);
            }
        }
        private async Task<string> Post(string uri)
        {
            using (var client = new HttpClient())
            using (var request = new HttpRequestMessage())
            {
                request.Method = HttpMethod.Post;
                request.RequestUri = new Uri(uri);
                request.Headers.Add("Ocp-Apim-Subscription-Key", key);

                var response = await client.SendAsync(request);
                if (response.IsSuccessStatusCode)
                {
                    return "{'result' : 'Success.'}";
                }
                else
                {
                    return await response.Content.ReadAsStringAsync();
                }
            }
        }
        private async Task<string> Put(string uri, String body)
        {
            using (var client = new HttpClient())
            using (var request = new HttpRequestMessage())
            {
                request.Method = HttpMethod.Put;
                request.RequestUri = new Uri(uri);
                request.Content = new StringContent(body, Encoding.UTF8, "application/json");
                request.Headers.Add("Ocp-Apim-Subscription-Key", key);

                var response = await client.SendAsync(request);
                if (response.IsSuccessStatusCode)
                {
                    return "{'result' : 'Success.'}";
                }
                else
                {
                    return await response.Content.ReadAsStringAsync();
                }
            }
        }
        private async Task<Response> Get(string uri)
        {
            using (var client = new HttpClient())
            using (var request = new HttpRequestMessage())
            {
                request.Method = HttpMethod.Get;
                request.RequestUri = new Uri(uri);
                request.Headers.Add("Ocp-Apim-Subscription-Key", key);

                var response = await client.SendAsync(request);
                var responseBody = await response.Content.ReadAsStringAsync();
                return new Response(response.Headers, responseBody);
            }
        }

        public async Task<Response> PostCreateKB(string kb)
        {
            string uri = host + "/knowledgebases/create";
            return await Post(uri, kb);
        }
        public async Task<Response> GetStatus(string operation)
        {
            string uri = host + operation;
            return await Get(uri);
        }
        public async Task<string> CreateKB(string knowledgeBase)
        {
            var data = await PostCreateKB(knowledgeBase);
            var operation = data.headers.GetValues("Location").First();

            var done = false;
            while (true != done)
            {
                data = await GetStatus(operation);

                var fields = JsonConvert.DeserializeObject<Dictionary<string, string>>(data.response);

                String state = fields["operationState"];
                if (state.CompareTo("Running") == 0 || state.CompareTo("NotStarted") == 0)
                {
                    var wait = data.headers.GetValues("Retry-After").First();
                    Thread.Sleep(Int32.Parse(wait) * 1000);
                }
                else
                {
                    done = true;
                }
            }
            return JsonConvert.DeserializeObject<Dictionary<string, string>>(data.response)["resourceLocation"].ToString();
        }
        public async Task UpdateKB(string kbLink, string knowledgeBase)
        {
            var uri = host + kbLink;
            var response = await Put(uri, knowledgeBase);
        }

        public async Task<object> GetKB(string kbLink)
        {
            var uri = host + kbLink + "/test/qna";
            var data = await Get(uri);
            return data.response;
        }
        public async Task PublishKB(string kbLink)
        {
            var uri = host + kbLink;
            var response = await Post(uri);
        }


    }
}
