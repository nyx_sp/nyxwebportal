﻿using System;
using System.Threading.Tasks;
using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using Microsoft.AspNetCore.Http;
using NyxWebPortal.Utils;

namespace NyxWebPortal.Services
{
    public class CloudinaryService
    {
        private static string _cloudinaryName;
        private static string _apiKey;
        private static string _secretKey;
    
        private Cloudinary cloudinary;

        public CloudinaryService()
        {
            Account account = new Account(_cloudinaryName, _apiKey, _secretKey);
            cloudinary = new Cloudinary(account);
        }
        
        public static void SetCredentials(string cloudinaryName, string apiKey, string secretKey)
        {
            _cloudinaryName = cloudinaryName;
            _apiKey = apiKey;
            _secretKey = secretKey;
        }

        public async Task<string> UploadAsync(string folder, string name, string tags, IFormFile formFile)
        {
            if (formFile == null)
            {
                return "https://res.cloudinary.com/dvpnpkbc8/image/upload/None.png";
            }

            ImageUploadParams iup = new ImageUploadParams
            {
                File = new FileDescription(name, formFile.OpenReadStream()),
                PublicId = $"{folder}/{name}_{RandomGenerator.GetStringAlphanumeric(6)}",
                Tags = tags,
                Overwrite = false
            };
            
            ImageUploadResult iur = await cloudinary.UploadAsync(iup);

            return iur.SecureUri.ToString();
        }
    }
}
