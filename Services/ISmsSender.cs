﻿using System;
using System.Threading.Tasks;

namespace NyxWebPortal.Services
{
    public interface ISmsSender
    {
        Task SendSmsAsync(string number, string message);
    }
}
