﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Antiforgery;
using Microsoft.AspNetCore.Http;
using NyxDatabase.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using NyxDatabase.Models;
using NyxWebPortal.Services;
using NyxWebPortal.Services.Firebase;
using Newtonsoft.Json;
using Hangfire;
using NyxWebPortal.WebHooks;

namespace NyxWebPortal
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            MessageServices.SetMailPassword(Configuration["MailPassword"]);
            CloudinaryService.SetCredentials(Configuration["CloudinaryName"],
                Configuration["CloudinaryApiKey"],
                Configuration["CloudinarySecretKey"]);
                
            string firebaseServiceAccuntJson = System.IO.File.ReadAllText(@"./secrets/FirebaseServiceAccount.json");
            ServiceAccountCredential sac = JsonConvert.DeserializeObject<ServiceAccountCredential>(firebaseServiceAccuntJson);
            
            // Add framework services.
            //services.AddDbContext<NyxDbContext>(options =>
                //options.UseSqlite(Configuration.GetConnectionString("DefaultConnection")));
            services.AddDbContext<NyxDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("AzureSqlServerDev"),
                    b => b.MigrationsAssembly(nameof(NyxWebPortal))));

            services.AddIdentity<NyxCustomer, IdentityRole>(config =>
            {
                config.SignIn.RequireConfirmedEmail = true;
            }).AddEntityFrameworkStores<NyxDbContext>()
            .AddDefaultTokenProviders();
        
            services.ConfigureApplicationCookie(options => options.LoginPath = "/Account/Login");

            services.AddMemoryCache();
        
            // Add framework services.
            services.AddMvc()
                .AddSessionStateTempDataProvider();

            services.AddSession(options =>
            {
                // Set a short timeout for easy testing.
                options.IdleTimeout = TimeSpan.FromMinutes(30);
                options.Cookie.HttpOnly = true;
            });
            
            services.AddScoped<CloudinaryService>();
            
            services.AddSingleton(sac);
            services.AddScoped(sp => new FirebaseService(Configuration["FirebaseServerApiKey"], (ServiceAccountCredential)sp.GetService(typeof(ServiceAccountCredential))));
            
            services.AddTransient<IEmailSender, MessageServices>();
            services.AddTransient<ISmsSender, MessageServices>();

            services.AddHangfire(x => x.UseSqlServerStorage("Server=tcp:hangfiretest.database.windows.net,1433;Initial Catalog=HangfireTest;Persist Security Info=False;User ID=soraino;Password=P@ssw0rd;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;"));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();
            
            app.UseSession();

            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            app.UseHangfireServer();
            app.UseHangfireDashboard();
            app.MapWhen(
                    c => c.Request.Path.Value.StartsWith("/webhooktest"),
                    a => a.UseStripeWebhookHandler()
                );
        }
    }
}
