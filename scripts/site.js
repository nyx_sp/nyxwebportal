﻿import Vue from 'vue'
import Buefy from 'buefy'
import components from '../components'
import axios from 'axios'
import moment from 'moment'
import VueFormWizard from 'vue-form-wizard'
const d3 = require('d3')

Vue.use(Buefy)
Vue.use(VueFormWizard)
Vue.component('chart', components.chart)

global.Vue = Vue
global.axios = axios
global.d3 = d3
global.moment = moment

console.log('works')

Date.prototype.addDays = function(days) {
  var dat = new Date(this.valueOf())
  dat.setDate(dat.getDate() + days)
  return dat
}