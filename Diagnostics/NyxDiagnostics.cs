﻿using System;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Web;
using Newtonsoft.Json;

namespace NyxBot.Diagnostics
{
    public class NyxDiagnostics
    {
        public static long DiagnosticChatId = 29777902;
        public static string DiagnosticKey = "207070529:AAHgVNAHgWNn5ubcDn2YmB8NPBFZsyf0yNo";
        public static async Task SendDiagnostic(string diagnosticMessage)
        {
            try
            {
                Debug.WriteLine(diagnosticMessage);
                await Send(diagnosticMessage);
                await LogToFirebase(diagnosticMessage);
            }
            catch (Exception ex)
            {
                Send(GetDiagosticMessage(ex));
                LogToFirebase(GetDiagosticMessage(ex));
            }
        }

        public static async Task SendDiagnostic(Exception ex)
        {
            try
            {
                await Send(GetDiagosticMessage(ex));
                await LogToFirebase(GetDiagosticMessage(ex));
            }
            catch (Exception exex)
            {
                Send(GetDiagosticMessage(exex));
                LogToFirebase(GetDiagosticMessage(exex));
            }
        }

        private async static Task Send(string text)
        {
            Debug.WriteLine(text);
            //Create the url
            string diagnosticBotUrl = $"api.telegram.org/bot{DiagnosticKey}/sendMessage";
            var builder = new UriBuilder(diagnosticBotUrl);
            builder.Port = -1;
            var query = HttpUtility.ParseQueryString(builder.Query);
            //string name = GlobalConfig.CurrentBotName != null ? GlobalConfig.CurrentBotName : GlobalConfig.BotId;
            string name = "PORTAL";
            query["text"] = $"{name}: {text}";

            //Send to Azeem
            query["chat_id"] = DiagnosticChatId.ToString();
            builder.Query = query.ToString();
            string url = builder.ToString();
            new HttpClient().GetAsync(url);

            //Send to David
            //query["chat_id"] = GlobalConfig.DavidDiagnosticChatId.ToString();
            //builder.Query = query.ToString();
            //string url = builder.ToString();
            //new HttpClient().GetAsync(url);
        }

        public static async Task LogFirebase(string diagnosticMessage)
        {
            try
            {
                await LogToFirebase(diagnosticMessage);
            }
            catch (Exception ex)
            {
                LogToFirebase(GetDiagosticMessage(ex));
            }
        }

        public static async Task LogFirebase(Exception ex)
        {
            try
            {
                await LogToFirebase(GetDiagosticMessage(ex));
            }
            catch (Exception exex)
            {
                LogToFirebase(GetDiagosticMessage(exex));
            }
        }

        private async static Task LogToFirebase(string text)
        {
            long epochTimeStamp = (long)DateTime.Now.Subtract(new DateTime(1970, 1, 1, 0, 0, 0)).TotalMilliseconds;
            //string logFirebaseUrl = $"{GlobalConfig.BaseFirebasePath}Log/{GlobalConfig.BotId}/{epochTimeStamp}.json";
            string x = JsonConvert.SerializeObject(new { text = text });
            //HttpResponseMessage res = await new HttpClient().PutAsync(logFirebaseUrl, new StringContent(x));
        }

        private static string GetDiagosticMessage(Exception ex)
        {
            string diagnosticMessage;
            diagnosticMessage = "------------------------------------";
            diagnosticMessage += "\n\nError: " + ex.Message;
            diagnosticMessage += "\n\nTrace: " + ex.StackTrace;
            diagnosticMessage += "\n\nSource: " + ex.Source;
            diagnosticMessage += "\n\nKey Data: " + ex.Data;
            diagnosticMessage += "\n\n------------------------------------";
            return diagnosticMessage;
        }
    }
}
