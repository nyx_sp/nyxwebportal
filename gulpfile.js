﻿// Gulp.js configuration
// modules
const 
    gulp = require('gulp'),
    glob = require('glob'),
    sass = require('gulp-sass'),
    postcss = require('gulp-postcss'),
    browserify = require('browserify'),
    babelify = require('babelify'),
    vueify = require('vueify'),
    assets = require('postcss-assets'),
    autoprefixer = require('autoprefixer'),
    mqpacker = require('css-mqpacker'),
    sassTildeImporter = require('node-sass-tilde-importer'),
    source = require('vinyl-source-stream'),
    cssnano = require('cssnano'),
    package = require('./package.json');


// development mode?
const devBuild = (process.env.NODE_ENV !== 'production')

  // folders
const folders = {
    src: 'src/',
    styles: 'styles/',
    scripts: 'scripts/',
    components: 'components/',
    wwwroot: 'wwwroot/dist/'
};

gulp.task('css', function() {
    let postCssOpts = [
        assets({ loadPaths: ['images/'] }),
        autoprefixer({ browsers: ['last 2 versions', '> 2%'] }),
        mqpacker
    ];
    
    if (!devBuild) {
        postCssOpts.push(cssnano);
    }
    
    return gulp.src(folders.styles + 'site.scss')
        .pipe(sass({
            importer: sassTildeImporter,
            outputStyle: 'nested',
            imagePath: 'images/',
            precision: 3,
            errLogToConsole: true
        }))
        .pipe(postcss(postCssOpts))
        .pipe(gulp.dest(folders.wwwroot));
});

gulp.task('js', function() {
    let entries = [folders.scripts + 'site.js'];
    browserify({
        entries: entries,
        debug: true,
        transform: [vueify, babelify]
    })
        .bundle()
        .pipe(source('site.js'))
        .pipe(gulp.dest(folders.wwwroot));

});