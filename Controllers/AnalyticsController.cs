﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NyxDatabase.Data;
using NyxDatabase.Models;
using NyxWebPortal.Services.Firebase;
using NyxWebPortal.Utils;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace NyxWebPortal.Controllers
{
    [Authorize(Roles = "CompanyAdmin")]
    public class Analytics : Controller
    {
        private readonly UserManager<NyxCustomer> _userManager;
        private readonly NyxDbContext _ctx;
        private readonly FirebaseService _firebaseService;
    
        public Analytics(
            UserManager<NyxCustomer> userManager,
            NyxDbContext ctx,
            FirebaseService firebaseService)
        {
            _userManager = userManager;
            _ctx = ctx;
            _firebaseService = firebaseService;
        }
        
        // GET: /<controller>/
        public async Task<IActionResult> Index(string id)
        {
            CompanyAdmin companyAdmin = await this.GetCompanyAdminAsync(_userManager, User);
            IdTokenResponse res = await _firebaseService.IdTokenAsync(companyAdmin._FirebaseRefreshToken);
            @ViewData["idToken"] = res.IdToken;

            Bot bot = await _ctx.Bots
                .SingleOrDefaultAsync(b => b.Id == id);
            
            return View(bot);
        }
    }
}
