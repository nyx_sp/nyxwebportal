﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NyxDatabase.Data;

namespace NyxWebPortal.Controllers
{
    public class TransactionController : Controller
    {
        private readonly NyxDbContext _ctx;

        public TransactionController(NyxDbContext ctx)
        {
            _ctx = ctx;
        }

        public IActionResult ViewAllTransaction()
        {
            return View();
        }
        public IActionResult ViewAllReserved()
        {
            return View();
        }


        //APIs
        [HttpGet("/[controller]/[action]/{eventId}")]
        public IActionResult getAllTransaction(string eventId)
        {
            try
            {
                List<object> transactions = new List<object>();
                var items = _ctx.Items
                    .Where(i => i.EventId == eventId)
                    .Select(i => new
                    {
                        i.Id
                    }
                    ).ToList();
                if (items.Count > 0)
                {
                    for (int i = 0; i < items.Count; i++)
                    {
                        var trans = _ctx.BotTransactions
                            .Where(transaction => transaction.ItemId == items[i].Id)
                            .OrderByDescending(transaction => transaction._CreatedOn)
                            .ToList();
                        transactions.AddRange(trans);
                    }
                    return Ok(transactions);
                }
                return BadRequest("No transaction found");
            }
            catch (Exception e)
            {
                return BadRequest("An error has occured");
            }
        }

        [HttpGet("/[controller]/[action]/{eventId}")]
        public IActionResult getAllReserved(string eventId)
        {
            try
            {
                List<object> transactions = new List<object>();
                var items = _ctx.Items
                    .Where(i => i.EventId == eventId)
                    .Select(i => new
                    {
                        i.Id
                    }
                    ).ToList();
                if (items.Count > 0)
                {
                    for (int i = 0; i < items.Count; i++)
                    {
                        var trans = _ctx.BotTransactions
                            .Where(transaction => transaction.ItemId == items[i].Id && transaction.Status == NyxDatabase.Models.BotTransactionStatus.Reserved)
                            .OrderByDescending(transaction => transaction._CreatedOn)
                            .ToList();
                        transactions.AddRange(trans);
                    }
                    return Ok(transactions);
                }
                return BadRequest("No transaction found");
            }
            catch (Exception e)
            {
                return BadRequest("An error has occured");
            }
        }
        [HttpPut("/[controller]/[action]/{Id}")]
        public async Task<IActionResult> putRedeemTransaction(string Id)
        {
            try
            {
                var trans = _ctx.BotTransactions.Where(i => i.Id == Id).Single();
                trans.Status = NyxDatabase.Models.BotTransactionStatus.Redeemed;
                trans.Id = null;
                _ctx.BotTransactions.Add(trans);
                await _ctx.SaveChangesAsync();
                return Ok("Transaction redeemed");
            }catch(Exception e)
            {
                return BadRequest("An error has occured");
            }
        }
    }
}