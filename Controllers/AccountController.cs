﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using NyxDatabase.Data;
using NyxDatabase.Models;
using NyxWebPortal.Models.Accounts;
using NyxWebPortal.Services;
using NyxWebPortal.Services.Firebase;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace NyxWebPortal.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private readonly NyxDbContext _ctx;
        private readonly UserManager<NyxCustomer> _userManager;
        private readonly SignInManager<NyxCustomer> _signInManager;
        private readonly IEmailSender _emailSender;
        private readonly ISmsSender _smsSender;
        private readonly ILogger _logger;
        private readonly CloudinaryService _cloudinary;
        private readonly FirebaseService _firebaseService;

        public const string cloudinaryFolder = "NyxCustomers";
        public const string companyAdminRole = "CompanyAdmin";
        public const string companyCustomerSupportRole = "CompanyCustomerSupport";

        public AccountController(
            NyxDbContext ctx,
            UserManager<NyxCustomer> userManager,
            SignInManager<NyxCustomer> signInManager,
            IEmailSender emailSender,
            ISmsSender smsSender,
            ILoggerFactory loggerFactory,
            CloudinaryService cloudinary,
            FirebaseService firebaseService)
        {
            _ctx = ctx;
            _userManager = userManager;
            _signInManager = signInManager;
            _emailSender = emailSender;
            _smsSender = smsSender;
            _logger = loggerFactory.CreateLogger<AccountController>();
            _cloudinary = cloudinary;
            _firebaseService = firebaseService;
        }
        // GET: /<controller>/
        public async Task<IActionResult> Index(string code = null)
        {
            CompanyAdmin admin = await LoadAdminAsync();

            string result = null;

            if (code != null)
            {
                try
                {

                    {
                        result = await StripeServices.StripeAccessToken(code);
                        JObject jObject = JObject.Parse(result);
                        var StripeId = (string)jObject["stripe_user_id"];

                        if (StripeId != null)
                        {
                            ////Just put somewhere and save it
                            //CookieOptions option = new CookieOptions();
                            //Response.Cookies.Append("StripeToken", StripeId, option);
                            admin.StripeConnectId = StripeId;
                            await _ctx.SaveChangesAsync();
                            TempData["Toast"] = "Your stripe account has been linked.";
                            TempData["ToastType"] = "is-success";
                            return View(admin);
                        }

                    }
                }
                catch (Exception)
                {
                    TempData["Toast"] = "Something went wrong please try again!";
                    TempData["ToastType"] = "is-danger";
                    return View(admin);
                }
            }
            return View(admin);
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Login(string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel vm, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            if (ModelState.IsValid)
            {
                // This doesn't count login failures towards account lockout
                // To enable password failures to trigger account lockout, set lockoutOnFailure: true
                var result = await _signInManager.PasswordSignInAsync(vm.Email, vm.Password, vm.RememberMe, lockoutOnFailure: false);
                if (result.Succeeded)
                {
                    _logger.LogInformation(1, "User logged in.");
                    NyxCustomer user = await _userManager.FindByEmailAsync(vm.Email);
                    if (!user._Active)
                    {
                        await _signInManager.SignOutAsync();
                        TempData["Toast"] = "Your account has been disabled";
                        TempData["ToastType"] = "is-danger";
                        return View(vm);
                    }

                    if (Url.IsLocalUrl(returnUrl))
                    {
                        return Redirect(returnUrl);
                    }
                    else if (user is CompanyAdmin)
                    {
                        return RedirectToAction(nameof(WorkspaceController.Index), "Workspace");
                    }
                    else if (user is CompanyCustomerSupport)
                    {
                        return RedirectToAction(nameof(CustomerSupportController.Index), "CustomerSupport");
                    }

                    return Redirect(returnUrl);
                }
                if (result.RequiresTwoFactor)
                {
                    //return RedirectToAction(nameof(SendCode), new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });
                }
                if (result.IsLockedOut)
                {
                    _logger.LogWarning(2, "User account locked out.");
                    return View("Lockout");
                }
                else
                {
                    TempData["Toast"] = "Invalid login attempt.";
                    TempData["ToastType"] = "is-danger";
                    return View(vm);
                }
            }

            // If we got this far, something failed, redisplay form
            TempData["Toast"] = "An unknown error occured.";
            TempData["ToastType"] = "is-danger";
            return View(vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            _logger.LogInformation(4, "User logged out.");
            return RedirectToAction(nameof(HomeController.Index), "Home");
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Register()
        {
            ViewData["Countries"] = _ctx.Countries.ToList();
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel vm)
        {
            TempData["ToastType"] = "is-danger";
            if (!ModelState.IsValid)
            {
                ViewData["Countries"] = _ctx.Countries.ToList();
                ModelErrorCollection errors = GetModelErrors();
                TempData["Toast"] = $"{errors.First().ErrorMessage}";
                return View(vm);
            }

            if (vm.Password != vm.ConfirmPassword)
            {
                ViewData["Countries"] = _ctx.Countries.ToList();
                TempData["Toast"] = $"The confirmation password does not match";
                return View(vm);
            }
            try
            {
                string customerId = null;
                if (!string.IsNullOrWhiteSpace(vm.StripeTransactionId))
                {
                    string customerCreationResult = await StripeServices.CreateCustomer(vm.StripeTransactionId, vm.AdminEmail);
                    JObject jObject = JObject.Parse(customerCreationResult);
                    var ErrorCheck = jObject["error"];
                    if (ErrorCheck == null)
                    {
                        customerId = jObject["id"].ToString();
                    }
                    else {
                        customerId = null;
                    }
                }

                CompanyAdmin user = new CompanyAdmin
                {
                    UserName = vm.AdminEmail,
                    Email = vm.AdminEmail,
                    FirstName = vm.AdminFirstName,
                    LastName = vm.AdminLastName,
                    PhoneCode = vm.AdminPhoneCode,
                    PhoneNumber = vm.AdminPhoneNumber,
                    StripeCustomerId = customerId,
                    //DateOfBirth = DateTime.ParseExact(vm.AdminDOB, "dd/MM/yyyy", CultureInfo.InvariantCulture),
                    _Active = true,
                    _CreatedOn = DateTimeOffset.UtcNow
                };

                Company company = new Company
                {
                    Name = vm.CompanyName,
                    CountryId = vm.CompanyCountry,
                    State = vm.CompanyState,
                    City = vm.CompanyCity,
                    AddressLine1 = vm.CompanyAddressLine1,
                    AddressLine2 = vm.CompanyAddressLine2,
                    PostalCode = vm.CompanyPostalCode,
                    PhoneCode = vm.CompanyPhoneCode,
                    PhoneNumber = vm.CompanyPhoneNumber,
                    _CreatedOn = DateTime.UtcNow,
                    _Active = true
                };

                IdentityResult result = await _userManager.CreateAsync(user, vm.Password);
                if (result.Succeeded)
                {
                    // send confirmation email
                    string code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                    string callbackUrl = Url.Action(nameof(ConfirmEmail),
                        "Account",
                        new { userId = user.Id, code = code },
                        protocol: HttpContext.Request.Scheme);
                    await _emailSender.SendEmailAsync(user.Email, "Confirm your account",
                            $"Please confirm your account by clicking this link: <a href=\"{callbackUrl}\">link</a>");

                    // register firebase account
                    string randomFirebasePassword = Utils.RandomGenerator.GetStringAlphanumeric(32);
                    // if there was an excpetion then firebase api probably changed
                    EmailSignUpResponse fbRes = await _firebaseService.EmailSignUpAsync(user.Email, randomFirebasePassword);
                    user._FirebaseRefreshToken = fbRes.RefreshToken;

                    user.ProfileImageUrl = await _cloudinary.UploadAsync(cloudinaryFolder, user.Email, user.Email, vm.AdminProfileImg);
                    _ctx.Companies.Add(company);
                    await _ctx.SaveChangesAsync();
                    user.CompanyId = company.Id;
                    company.Admin = user;
                    await _ctx.SaveChangesAsync();
                    await _userManager.AddToRoleAsync(user, companyAdminRole);
                    _logger.LogInformation(3, "User created a new account with password.");

                    return RedirectToAction(nameof(EmailNotConfirmed), new { email = vm.AdminEmail });
                }
            }
            catch (Exception e)
            {
                return View(vm);
            }
            ViewData["Countries"] = _ctx.Countries.ToList();
            return View(vm);
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult EmailNotConfirmed(string email = null)
        {
            ViewData["email"] = email;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EmailNotConfirmed(int a, string email = null)
        {
            TempData["Toast"] = "Please check your email for a verification link.";
            TempData["ToastType"] = "is-success";

            NyxCustomer user = await _userManager.FindByEmailAsync(email);

            if (user == null)
                return RedirectToAction(nameof(Login));

            if (await _userManager.IsEmailConfirmedAsync(user))
            {
                TempData["Toast"] = "Email has already been verified.";
                TempData["ToastType"] = "is-warning";
                ViewData["email"] = email;
                return RedirectToAction(nameof(Login));
            }
            string code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
            string callbackUrl = Url.Action(nameof(ConfirmEmail),
                "Account",
                new { userId = user.Id, code = code },
                protocol: HttpContext.Request.Scheme);
            await _emailSender.SendEmailAsync(user.Email, "Confirm your account",
                    $"Please confirm your account by clicking this link: <a href=\"{callbackUrl}\">link</a>");

            return RedirectToAction(nameof(Login));
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                return View("Error");
            }
            var result = await _userManager.ConfirmEmailAsync(user, code);
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        [HttpGet]
        public async Task<IActionResult> ChangeProfileImage()
        {
            NyxCustomer user = await _userManager.GetUserAsync(User);
            ViewData["CurrentImage"] = user.ProfileImageUrl;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ChangeProfileImage(ChangeProfileImageViewModel vm)
        {
            NyxCustomer user = await _userManager.GetUserAsync(User);
            ViewData["CurrentImage"] = user.ProfileImageUrl;

            _ctx.Attach(user);
            user.ProfileImageUrl = await _cloudinary.UploadAsync(cloudinaryFolder, user.Email, user.Email, vm.Image);
            await _ctx.SaveChangesAsync();

            TempData["Toast"] = "Successfully changed profile image.";
            TempData["ToastType"] = "is-success";
            return RedirectToAction(nameof(Index));
        }

        [HttpGet]
        public async Task<IActionResult> EditProfile()
        {
            CompanyAdmin user = await LoadAdminAsync();
            return View(new EditProfileViewModel
            {
                FirstName = user.FirstName,
                LastName = user.LastName,
                DOB = user.DateOfBirth.Date.ToString(),
                ImgUrl = user.ProfileImageUrl,
                PhoneCode = user.PhoneCode,
                PhoneNumber = user.PhoneNumber,
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditProfile(EditProfileViewModel vm)
        {
            CompanyAdmin user = await LoadAdminAsync();
            try
            {
                _ctx.Attach(user);
                if (vm.Image != null)
                {
                    user.ProfileImageUrl = await _cloudinary.UploadAsync(cloudinaryFolder, user.Email, user.Email, vm.Image);
                }
                user.FirstName = vm.FirstName;
                user.LastName = vm.LastName;
                user.PhoneNumber = vm.PhoneNumber;
                user.PhoneCode = vm.PhoneCode;
                user.DateOfBirth = DateTime.ParseExact(vm.DOB, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                await _ctx.SaveChangesAsync();

                TempData["Toast"] = "Your profile info has been updated.";
                TempData["ToastType"] = "is-success";
                return RedirectToAction(nameof(Index));
            }
            catch (Exception e)
            {
                return View(vm);
            }
        }

        [HttpGet]
        public IActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ChangePassword(ChangePasswordViewModel vm)
        {
            TempData["ToastType"] = "is-danger";
            if (vm.NewPassword != vm.ConfirmPassword)
            {
                TempData["Toast"] = "Your new password does not match.";
                return View();
            }

            NyxCustomer user = await _userManager.GetUserAsync(User);
            var signInResult = await _signInManager.CheckPasswordSignInAsync(user, vm.CurrentPassword, false);
            if (!signInResult.Succeeded)
            {
                TempData["Toast"] = "Your current password was wrong.";
                return View();
            }

            if (vm.CurrentPassword == vm.NewPassword)
            {
                TempData["Toast"] = "Your new password cannot be the same as the current one.";
                return View();
            }

            string token = await _userManager.GeneratePasswordResetTokenAsync(user);

            var changeResult = await _userManager.ResetPasswordAsync(user, token, vm.NewPassword);

            if (!changeResult.Succeeded)
            {
                TempData["Toast"] = $"{changeResult.Errors.First().Description}";
                return View();
            }

            TempData["Toast"] = "Password successfully changed.";
            TempData["ToastType"] = "is-success";
            return RedirectToAction(nameof(Index));
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ForgotPassword(string email)
        {
            TempData["ToastType"] = "is-danger";
            NyxCustomer user = await _userManager.FindByEmailAsync(email);

            if (user == null)
            {
                TempData["Toast"] = "No such email found.";
                return View();
            }

            if (!await _userManager.IsEmailConfirmedAsync(user))
            {
                TempData["Toast"] = "Email has not been verfied.";
                return View();
            }

            string code = await _userManager.GeneratePasswordResetTokenAsync(user);
            string callbackUrl = Url.Action(nameof(ResetPassword),
                "Account",
                new { userId = user.Id, code = code },
                protocol: HttpContext.Request.Scheme);
            await _emailSender.SendEmailAsync(user.Email, "Reset your password",
                    $"Please reset your password by clicking this link: <a href=\"{callbackUrl}\">link</a>");

            TempData["Toast"] = "Please check your email for a reset password link";
            TempData["ToastType"] = null;
            return RedirectToAction(nameof(Login));
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ResetPassword(string userId, string code = null)
        {
            return userId == null || code == null ?
                View("Error") :
                View(new ResetPasswordViewModel { Id = userId, Code = code });
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ResetPassword(ResetPasswordViewModel vm)
        {
            TempData["ToastType"] = "is-danger";
            if (!ModelState.IsValid)
            {
                return View(vm);
            }
            NyxCustomer user = await _userManager.FindByIdAsync(vm.Id);
            if (user == null)
                return RedirectToAction(nameof(Login));

            if (user.Email != vm.Email)
                return RedirectToAction(nameof(Login));

            if (vm.NewPassword != vm.ConfirmPassword)
            {
                TempData["Toast"] = "Your new password does not match.";
                vm.Email = null;
                vm.NewPassword = null;
                vm.ConfirmPassword = null;
                return View(vm);
            }

            var result = await _userManager.ResetPasswordAsync(user, vm.Code, vm.NewPassword);
            if (result.Succeeded)
            {
                TempData["Toast"] = "Your password has been reset.";
                TempData["ToastType"] = "is-success";
                return RedirectToAction(nameof(Login));
            }
            TempData["Toast"] = result.Errors.First().Description;

            vm.Email = null;
            vm.NewPassword = null;
            vm.ConfirmPassword = null;
            return View(vm);
        }

        [HttpGet]
        [Authorize(Roles = "CompanyAdmin")]
        public async Task<IActionResult> EditCompany()
        {
            CompanyAdmin admin = await LoadAdminAsync();
            ViewData["Countries"] = _ctx.Countries.ToList();
            EditCompanyViewModel vm = new EditCompanyViewModel
            {
                Name = admin.Company.Name,
                Country = admin.Company.CountryId,
                State = admin.Company.State,
                City = admin.Company.City,
                AddressLine1 = admin.Company.AddressLine1,
                AddressLine2 = admin.Company.AddressLine2,
                PostalCode = admin.Company.PostalCode,
                PhoneCode = admin.Company.PhoneCode,
                PhoneNumber = admin.Company.PhoneNumber
            };
            return View(vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "CompanyAdmin")]
        public async Task<IActionResult> EditCompany(EditCompanyViewModel vm)
        {
            CompanyAdmin admin = await LoadAdminAsync();
            _ctx.Attach(admin);

            admin.Company.Name = vm.Name;
            admin.Company.CountryId = vm.Country;
            admin.Company.State = vm.State;
            admin.Company.City = vm.City;
            admin.Company.AddressLine1 = vm.AddressLine1;
            admin.Company.AddressLine2 = vm.AddressLine2;
            admin.Company.PostalCode = vm.PostalCode;
            admin.Company.PhoneCode = vm.PhoneCode;
            admin.Company.PhoneNumber = vm.PhoneNumber;
            await _ctx.SaveChangesAsync();

            TempData["Toast"] = "Your company info has been updated.";
            TempData["ToastType"] = "is-success";
            return RedirectToAction(nameof(Index));
        }

        private async Task<CompanyAdmin> LoadAdminAsync()
        {
            CompanyAdmin companyAdmin = (CompanyAdmin)await _userManager.GetUserAsync(User);
            _ctx.Entry(companyAdmin).Reference(m => m.Company).Load();
            _ctx.Entry(companyAdmin.Company).Collection(m => m.CustomerSupports).Load();

            return companyAdmin;
        }

        private ModelErrorCollection GetModelErrors()
        {
            return ModelState.Values
                    .Select(v => v.Errors)
                    .Aggregate(new ModelErrorCollection(), (allErrors, vErrors) =>
                    {
                        foreach (ModelError modelError in vErrors)
                        {
                            allErrors.Add(modelError);
                        }
                        return allErrors;
                    });
        }

        [HttpPost]
        public async Task TestPay(string someText)
        {
            //string testcode = Request.Cookies["StripeToken"];
            CompanyAdmin admin = await LoadAdminAsync();
            string result = "";
            result = await StripeServices.StripePaymentTest(admin.StripeConnectId, someText);
            JObject jObject = JObject.Parse(result);
            var ErrorCheck = jObject["error"];
            if (ErrorCheck == null)
            {
                ModelState.Clear();
                //await Index(null, "Test Payment is successful", false);
            }
            else
            {

                admin.StripeConnectId = null;
                await _ctx.SaveChangesAsync();
                ModelState.Clear();
                string msg = (string)ErrorCheck.SelectToken("message");
                //await Index(null, msg, true);
            }
        }
    }
}
