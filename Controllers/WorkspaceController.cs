﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using NyxDatabase.Data;
using NyxDatabase.Models;
using NyxDatabase.Models.ManyToMany;
using NyxWebPortal.Models.Workspace;
using NyxWebPortal.Services;
using NyxWebPortal.Services.Firebase;
using NyxWebPortal.Utils;
using NyxBot.Diagnostics;
using System.Net;
using System.IO;
using Newtonsoft.Json.Linq;
// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace NyxWebPortal.Controllers
{
    [Authorize(Roles = AccountController.companyAdminRole)]
    public class WorkspaceController : Controller
    {
        private readonly UserManager<NyxCustomer> _userManager;
        private readonly NyxDbContext _ctx;
        private readonly CloudinaryService _cloudinary;
        private readonly FirebaseService _firebaseService;
        private readonly IEmailSender _emailSender;
        private readonly ILogger _logger;

        public WorkspaceController(
            UserManager<NyxCustomer> userManager,
            NyxDbContext ctx,
            CloudinaryService cloudinary,
            FirebaseService firebaseService,
            IEmailSender emailSender,
            ILoggerFactory loggerFactory)
        {
            _userManager = userManager;
            _ctx = ctx;
            _cloudinary = cloudinary;
            _firebaseService = firebaseService;
            _emailSender = emailSender;
            _logger = loggerFactory.CreateLogger<AccountController>();
        }

        public const string BotCloudinaryFolder = "bot";

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> CreateBot()
        {
            BotViewModel vm = new BotViewModel();
            vm.Plans = await _ctx.Plans.ToListAsync();
            vm.Countries = await _ctx.Countries.Select(c => c.Id).ToListAsync();
            vm.StripeCustomerId = (await this.GetCompanyAdminAsync(_userManager, User)).StripeCustomerId;
            return View(vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateGrcBot(GrcBotViewModel vm)
        {
            CompanyAdmin companyAdmin = await this.GetCompanyAdminAsync(_userManager, User);
            string stripeBotSubId = "";

            BotViewModel toSendBack = new BotViewModel();
            toSendBack.Plans = await _ctx.Plans.ToListAsync();
            toSendBack.Countries = await _ctx.Countries.Select(c => c.Id).ToListAsync();
            toSendBack.StripeCustomerId = (await this.GetCompanyAdminAsync(_userManager, User)).StripeCustomerId;

            if (vm.StripeCustomerId != "" && vm.StripeCustomerId != null)
            {
                if (companyAdmin.StripeCustomerId != vm.StripeCustomerId)
                {
                    string result = await StripeServices.CreateCustomer(vm.StripeCustomerId, companyAdmin.Email);
                    JObject jObject = JObject.Parse(result);
                    var ErrorCheck = jObject["error"];
                    if (ErrorCheck == null)
                    {
                        companyAdmin.StripeCustomerId = jObject["id"].ToString();
                        _ctx.CompanyAdmins.Update(companyAdmin);

                        result = await StripeServices.Subscribe(companyAdmin.StripeCustomerId, vm.StripePlanId);
                        jObject = JObject.Parse(result);
                        ErrorCheck = jObject["error"];
                        if (ErrorCheck == null)
                        {
                            stripeBotSubId = jObject["id"].ToString();
                        }
                        else
                        {

                            TempData["Toast"] = $"Something went worng with Stripe!";
                            TempData["ToastType"] = "is-success";
                            return RedirectToAction(nameof(CreateBot));
                        }
                    }
                    else
                    {
                        TempData["Toast"] = $"Something went worng with Stripe!";
                        TempData["ToastType"] = "is-success";
                        return RedirectToAction(nameof(CreateBot));
                    }

                }
                else
                {
                    string result = await StripeServices.Subscribe(companyAdmin.StripeCustomerId, vm.StripePlanId);
                    JObject jObject = JObject.Parse(result);
                    var ErrorCheck = jObject["error"];

                    ErrorCheck = jObject["error"];
                    if (ErrorCheck == null)
                    {
                        stripeBotSubId = jObject["id"].ToString();
                    }
                    else
                    {
                        TempData["Toast"] = $"Something went worng with your Stripe!";
                        TempData["ToastType"] = "is-danger";
                        return RedirectToAction(nameof(CreateBot));
                    }

                }
            }
            else {
                TempData["Toast"] = $"Something went worng!";
                TempData["ToastType"] = "is-danger";
                return RedirectToAction(nameof(CreateBot));
            }

            // Add to db
            GRCBot grcBot = new GRCBot();
            grcBot.Id = Utils.RandomGenerator.GetStringAlphanumeric(16);
            grcBot.Name = vm.Name;
            grcBot.Type = vm.Type;
            grcBot.PlanId = vm.PlanId;
            grcBot.Address = new Address();
            grcBot.Address.BotId = grcBot.Id;
            grcBot.Address.AddressLine1 = vm.AddressLine1;
            grcBot.Address.AddressLine2 = vm.AddressLine2;
            grcBot.Address.PostalCode = vm.PostalCode;
            grcBot.CompanyId = companyAdmin.CompanyId;
            grcBot.VolunteerUrl = vm.VolunteerUrl;
            grcBot.WelcomeMessage = vm.WelcomeMessage;
            grcBot.NoAnswerFoundMessage = vm.NoAnswerFoundMessage;
            grcBot.RequestContactDetailsMessage = vm.RequestContactDetailsMessage;
            grcBot.UserAgreeContactMessage = vm.UserAgreeContactMessage;
            grcBot.UserDisagreeContactMessage = vm.UserDisagreeContactMessage;
            grcBot.UserSwearMessage = vm.UserSwearMessage;
            grcBot.AskUserUsefulMessage = vm.AskUserUsefulMessage;
            grcBot.AskUserUsefulPercentage = vm.AskUserUsefulPercentage;
            grcBot.Status = BotStatus.CollectedAdminData;
            grcBot.TeamInfoImageUrl = await _cloudinary.UploadAsync(grcBot.Id, grcBot.Id, grcBot.Id, vm.TeamImage);
            grcBot.StripeSubscriptionId = stripeBotSubId;
            grcBot._Active = false;
            grcBot._CreatedOn = DateTime.UtcNow;
            await _ctx.GRCBots.AddAsync(grcBot);

            // Create firebase and config firebase whitelist
            await _firebaseService.CreateBotAsync(companyAdmin, grcBot);

            await _ctx.SaveChangesAsync();

            // Redirect to GRC edit page
            return RedirectToAction(nameof(Details), new { id = grcBot.Id });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> UpdateTeamInfo(string BotId, GrcBotViewModel vm){

            if (!ModelState.IsValid)
            {
                return View();
            }

            CompanyAdmin companyAdmin = await this.GetCompanyAdminAsync(_userManager, User);
            GRCBot bot = await _ctx.GRCBots.SingleOrDefaultAsync(b => b.Id == BotId && b.CompanyId == companyAdmin.CompanyId);

            if (bot == null)
                return RedirectToAction(nameof(Bots));

            bot.TeamInfoImageUrl = await _cloudinary.UploadAsync(BotId, BotId, BotId, vm.TeamImage);

            await _ctx.SaveChangesAsync();

            return RedirectToAction(nameof(Details), new { id = BotId });
        }
        //[HttpPost]
        //public IActionResult CreateShopBot(ShopBotViewModel vm)
        //{

        //    return View();
        //}

        [HttpGet]
        public async Task<IActionResult> Bots()
        {
            CompanyAdmin companyAdmin = await this.GetCompanyAdminAsync(_userManager, User);
            List<Bot> bots = new List<Bot>();
                bots = await _ctx.Bots
                   .Where(b => b.CompanyId == companyAdmin.CompanyId)
                   .ToListAsync();
            //Bot b = new Bot();
            return View(bots);
        }

        [HttpGet]
        public async Task<IActionResult> Details(string id)
        {
            CompanyAdmin companyAdmin = await this.GetCompanyAdminAsync(_userManager, User);
            Bot bot = await _ctx.GRCBots.SingleOrDefaultAsync(b => b.Id == id && b.CompanyId == companyAdmin.CompanyId);

            if (bot == null)
                return RedirectToAction(nameof(Bots));

            if (bot is GRCBot)
            {
                GRCBot grcBot = await _ctx.GRCBots
                    .Include(b => b.Plan)
                    .Include(b => b.Address)
                    .SingleOrDefaultAsync(b => b.Id == id && b.CompanyId == companyAdmin.CompanyId);
                return View("ViewGrc", grcBot);
            }

            return null;
        }

        [HttpGet]
        public async Task<IActionResult> EditBotDetails(string id)
        {
            CompanyAdmin companyAdmin = await this.GetCompanyAdminAsync(_userManager, User);

            Bot bot = await _ctx.Bots
                .SingleOrDefaultAsync(b => b.Id == id && b.CompanyId == companyAdmin.CompanyId);

            if (bot == null)
                return RedirectToAction(nameof(Bots));


            if (bot is GRCBot)
            {
                GRCBot grcbot = await _ctx.GRCBots
                    .Include(b => b.Address)
                    .SingleOrDefaultAsync(b => b.Id == id && b.CompanyId == companyAdmin.CompanyId);

                GrcBotViewModel vm = new GrcBotViewModel
                {
                    Id = grcbot.Id,
                    Name = grcbot.Name,
                    TeamImageUrl = grcbot.TeamInfoImageUrl,
                    WelcomeMessage = grcbot.WelcomeMessage,
                    NoAnswerFoundMessage = grcbot.NoAnswerFoundMessage,
                    RequestContactDetailsMessage = grcbot.RequestContactDetailsMessage,
                    UserAgreeContactMessage = grcbot.UserAgreeContactMessage,
                    UserDisagreeContactMessage = grcbot.UserDisagreeContactMessage,
                    UserSwearMessage = grcbot.UserSwearMessage,
                    AskUserUsefulMessage = grcbot.AskUserUsefulMessage,
                    AskUserUsefulPercentage = grcbot.AskUserUsefulPercentage,
                    AddressLine1 = grcbot.Address.AddressLine1,
                    AddressLine2 = grcbot.Address.AddressLine2,
                    PostalCode = grcbot.Address.PostalCode,
                    StripePlanId = await StripeServices.GetPlanIdOnSubId(grcbot.StripeSubscriptionId)
                };
                vm.Plans = await _ctx.Plans.ToListAsync();

                return View("EditGRCBot", vm);
            }

            return Redirect(nameof(Bots));
        }

        [HttpPost]
        public async Task<IActionResult> ChangeBotPlan(string id, string StripePlanId)
        {
            CompanyAdmin companyAdmin = await this.GetCompanyAdminAsync(_userManager, User);

            GRCBot bot = await _ctx.GRCBots
                .Include(b => b.Address)
                .SingleOrDefaultAsync(b => b.Id == id && b.CompanyId == companyAdmin.CompanyId);

            string result = await StripeServices.ChangeSubscription(bot.StripeSubscriptionId, StripePlanId);

            JObject jObject = JObject.Parse(result);
            var ErrorCheck = jObject["error"];
            if (ErrorCheck == null)
            {
                TempData["Toast"] = $"Change Successfully!";
                TempData["ToastType"] = "is-success";
                return RedirectToAction(nameof(Details), new { id = bot.Id });
            }
            else
            {
                TempData["Toast"] = $"Something went worng with Stripe!";
                TempData["ToastType"] = "is-danger";
                return RedirectToAction(nameof(Details), new { id = bot.Id });
            }

        }

        [HttpPost]
        public async Task<IActionResult> EditGRCBot(string id, GrcBotViewModel vm)
        {
            CompanyAdmin companyAdmin = await this.GetCompanyAdminAsync(_userManager, User);

            GRCBot bot = await _ctx.GRCBots
                .Include(b => b.Address)
                .SingleOrDefaultAsync(b => b.Id == id && b.CompanyId == companyAdmin.CompanyId);

            if (bot == null)
                return RedirectToAction(nameof(Bots));

            bot.Name = vm.Name;
            bot.WelcomeMessage = vm.WelcomeMessage;
            bot.NoAnswerFoundMessage = vm.NoAnswerFoundMessage;
            bot.RequestContactDetailsMessage = vm.RequestContactDetailsMessage;
            bot.UserAgreeContactMessage = vm.UserAgreeContactMessage;
            bot.UserDisagreeContactMessage = vm.UserDisagreeContactMessage;
            bot.UserSwearMessage = vm.UserSwearMessage;
            bot.AskUserUsefulMessage = vm.AskUserUsefulMessage;
            bot.AskUserUsefulPercentage = vm.AskUserUsefulPercentage;
            bot.Address.AddressLine1 = vm.AddressLine1;
            bot.Address.AddressLine2 = vm.AddressLine2;
            bot.Address.PostalCode = vm.PostalCode;
            
            if (vm.TeamImage != null)
            {
                // need to check file type
                bot.TeamInfoImageUrl = await _cloudinary.UploadAsync(BotCloudinaryFolder, $"{bot.Id}-TeamImage", "{bot.Id, Team image}", vm.TeamImage);
            }

            await _ctx.SaveChangesAsync();
            
            return RedirectToAction(nameof(Details), new { id = bot.Id });
        }
        
        [HttpGet]
        public async Task<IActionResult> EditBotEvent(string id)
        {
            Event ee = await _ctx.Events.Include(b => b.Tickets).SingleOrDefaultAsync(e => e.Id == id);
            if (ee == null)
                return RedirectToAction(nameof(Bots));

            return View(ee);
        }
        
        [HttpGet]
        public async Task<IActionResult> AddBotEvent(string id)
        {
            CompanyAdmin companyAdmin = await this.GetCompanyAdminAsync(_userManager, User);
            GRCBot bot = await _ctx.GRCBots
                .SingleOrDefaultAsync(b => b.Id == id && b.CompanyId == companyAdmin.CompanyId);

            if (bot == null)
                return RedirectToAction(nameof(Bots));

            EventViewModel vm = new EventViewModel
            {
                BotId = bot.Id
            };

            return View(vm);
        }
        [HttpGet]
        public async Task<IActionResult> AddBotEventItem(string id)
        {
            Event ee = await _ctx.Events.Include(b => b.Tickets).SingleOrDefaultAsync(e => e.Id == id);
            if (ee == null)
                return RedirectToAction(nameof(Bots));

            return View(ee);
        }
        [HttpGet]
        public async Task<IActionResult> EditBotEventItem(string id, string eventId)
        {
            CompanyAdmin companyAdmin = await this.GetCompanyAdminAsync(_userManager, User);
            Item item = await _ctx.Items.SingleOrDefaultAsync(e => e.Id == id);

            if (item == null)
                return RedirectToAction(nameof(Bots));

            var listingSold = await _ctx.BotTransactions
             .Where(
                 bt => bt.Status != BotTransactionStatus.Cancelled
                 && bt.Status != BotTransactionStatus.Pending && bt.ItemId == item.Id).ToListAsync();

            int sold = 0;

            foreach (var oneTransaction in listingSold)
            {
                sold += oneTransaction.Quantity;
            }

            Event eevent = await _ctx.Events.Where(ee => ee.Id == eventId).FirstOrDefaultAsync();
            if (eevent.Status == EventStatus.Drafting) {
                sold = 0;
            }

            ItemViewModel itemViewModel = new ItemViewModel();
            itemViewModel.Id = item.Id;
            itemViewModel.Name = item.Name;
            itemViewModel.NumberOfItems = item.NumberOfItems;
            itemViewModel.Price = item.Price;
            itemViewModel.Description = item.Description;
            itemViewModel.MaxReserveHoldDays = item.MaxReserveHoldDays;
            itemViewModel.MaxReservablePerOrder = item.MaxReservablePerOrder;
            itemViewModel.IsReservable = item.IsReservable;
            itemViewModel.EventId = eventId;
            itemViewModel.StartDate = item.StartDate;
            itemViewModel.EndDate = item.EndDate;
            itemViewModel.TicketsSold = sold;
            return View(itemViewModel);
        }
        [HttpPost]
        public async Task<IActionResult> AddBotEventItem(string id, EventItemViewModel vm)
        {
            CompanyAdmin companyAdmin = await this.GetCompanyAdminAsync(_userManager, User);
            Event eevent = await _ctx.Events.Include(b => b.Tickets).SingleOrDefaultAsync(e => e.Id == id);

            if (eevent == null)
                return RedirectToAction(nameof(Bots));

            Item item = new Item();
            item.Name = vm.Name;
            item.Price = vm.TicketPrice.Value;
            item.NumberOfItems = vm.NumberOfTickets.Value;
            item.Description = vm.Description;
            item.MaxReserveHoldDays = vm.MaxReserveHoldDays;
            item.MaxReservablePerOrder = vm.MaxReservablePerOrder;
            item.IsReservable = vm.IsReservable;

            item.StartDate = DateTimeOffset.ParseExact($"{vm.StartDateTime}", "MM/dd/yyyy HH:mm", CultureInfo.InvariantCulture).UtcDateTime;
            item.EndDate = DateTimeOffset.ParseExact($"{vm.EndDateTime}", "MM/dd/yyyy HH:mm", CultureInfo.InvariantCulture).UtcDateTime;

            _ctx.Items.Add(item);
            eevent.Tickets.Add(item);
            await _ctx.SaveChangesAsync();

            return RedirectToAction(nameof(EditBotEvent), new { id = id });
        }
        [HttpPost]
        public async Task<IActionResult> EditBotEventItem(string id, string eventId, EventItemViewModel eventItem)
        {
            CompanyAdmin companyAdmin = await this.GetCompanyAdminAsync(_userManager, User);
            Item item = await _ctx.Items.SingleOrDefaultAsync(e => e.Id == id);
            //await NyxDiagnostics.SendDiagnostic($"{eventItem.StartDate} {eventItem.StartTime}");

            if (item == null)
                return RedirectToAction(nameof(Bots));

            item.Name = eventItem.Name;
            item.Price = eventItem.TicketPrice.Value;
            item.NumberOfItems = eventItem.NumberOfTickets.Value;
            item.Description = eventItem.Description;
            item.MaxReserveHoldDays = eventItem.MaxReserveHoldDays;
            item.MaxReservablePerOrder = eventItem.MaxReservablePerOrder;
            item.IsReservable = eventItem.IsReservable;

            item.StartDate = DateTimeOffset.ParseExact($"{eventItem.StartDateTime}", "MM/dd/yyyy HH:mm", CultureInfo.InvariantCulture).UtcDateTime;
            item.EndDate = DateTimeOffset.ParseExact($"{eventItem.EndDateTime}", "MM/dd/yyyy HH:mm", CultureInfo.InvariantCulture).UtcDateTime;

            await _ctx.SaveChangesAsync();

            return RedirectToAction(nameof(EditBotEvent), new { id = eventId });
        }


        [HttpPost]
        public async Task<IActionResult> DeleteBotEventItem(string id, string eventId)
        {
            CompanyAdmin companyAdmin = await this.GetCompanyAdminAsync(_userManager, User);
            Item item = await _ctx.Items.SingleOrDefaultAsync(e => e.Id == id);
            
            if (item == null)
                return RedirectToAction(nameof(Bots));

            item.NumberOfItems = 0;
            await _ctx.SaveChangesAsync();

            return RedirectToAction(nameof(EditBotEvent), new { id = eventId });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ChangeEventStatus(string id, string Status)
        {
            CompanyAdmin companyAdmin = await this.GetCompanyAdminAsync(_userManager, User);

            Event selectedEvent = await _ctx.Events.Include(b => b.Tickets).SingleOrDefaultAsync(e => e.Id == id);

            if (selectedEvent == null)
                return RedirectToAction(nameof(Bots));

            switch (Status) {
                case "Start":
                    selectedEvent.Status = EventStatus.Started;
                    break;
                case "End":
                    selectedEvent.Status = EventStatus.Ended;
                    break;
                case "Drafting":
                    selectedEvent.Status = EventStatus.Drafting;
                    break;
                case "Cancel":
                    selectedEvent.Status = EventStatus.Cancelled;
                    foreach (var x in selectedEvent.Tickets) {
                        x.NumberOfItems = 0;
                    }
                    break;
            }
            await _ctx.SaveChangesAsync();
            return RedirectToAction(nameof(Details), new { id = selectedEvent.BotId });
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SendAnnouncement(string BotId, string Message)
        {
            if (!ModelState.IsValid)
            {
                return View(BotId);
            }
            // Hardcore to GRCbot for adding events to bot
            CompanyAdmin companyAdmin = await this.GetCompanyAdminAsync(_userManager, User);
            GRCBot bot = await _ctx.GRCBots.SingleOrDefaultAsync(b => b.Id == BotId && b.CompanyId == companyAdmin.CompanyId);

            if (bot == null)
                return RedirectToAction(nameof(Bots));

            //var httpWebRequest = (HttpWebRequest)WebRequest.Create(eevent.Bot.Url+"/api/SendAnnouncement");
            var httpWebRequest = (HttpWebRequest)WebRequest.Create("http://rc-bot-demo-4.azurewebsites.net/api/SendAnnouncement");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                var splitMsg = Message.Split("\r\n");
                Message = "";
                foreach (var oneMsg in splitMsg) {
                    Message += oneMsg + "\n\n";
                }

                string json = "{\"AnnouncementText\":\""+ Message + "\"}";

                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = await httpWebRequest.GetResponseAsync();

            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                var announcements = bot.Annoucements;
                var oneAnouncement = new Annoucement();

                if (announcements == null)
                {
                    announcements = new List<Annoucement>();
                    oneAnouncement.Bot = bot;
                    oneAnouncement.BotId = BotId;
                    oneAnouncement.Text = Message;
                    announcements.Add(oneAnouncement);
                }
                else
                {
                    oneAnouncement.Bot = bot;
                    oneAnouncement.BotId = BotId;
                    oneAnouncement.Text = Message;
                    announcements.Add(oneAnouncement);
                }
                await _ctx.SaveChangesAsync();
                return RedirectToAction(nameof(Details), new { id = BotId });
            }
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditEventImage(string id, EventViewModel vm)
        {
            if (!ModelState.IsValid)
            {
                vm.BotId = id;
                return View(vm);
            }
            // Hardcore to GRCbot for adding events to bot
            CompanyAdmin companyAdmin = await this.GetCompanyAdminAsync(_userManager, User);
            Event eevent = await _ctx.Events.Include(b => b.Tickets).SingleOrDefaultAsync(e => e.Id == id);

            if (eevent == null)
                return RedirectToAction(nameof(Bots));

            eevent.ImageUrl = await _cloudinary.UploadAsync(id, id, id, vm.EventImage);

            await _ctx.SaveChangesAsync();

            return RedirectToAction(nameof(EditBotEvent), new { id = id });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddBotEvent(string id, EventViewModel vm)
        {
            if (!ModelState.IsValid)
            {
                vm.BotId = id;
                return View(vm);
            }
            // Hardcore to GRCbot for adding events to bot
            CompanyAdmin companyAdmin = await this.GetCompanyAdminAsync(_userManager, User);
            Event eevent = new Event();
            GRCBot bot = await _ctx.GRCBots
                .Include(b => b.Events)
                .SingleOrDefaultAsync(b => b.Id == id && b.CompanyId == companyAdmin.CompanyId);

            if (bot == null)
                return RedirectToAction(nameof(Bots));
            eevent.BotId = id;
            eevent.Name = vm.Name;
            eevent.Description = vm.Description;
            eevent.Location = vm.Location;
            if (vm.EventStatus == "Draft")
            {
                eevent.Status = EventStatus.Drafting;
            }
            else if (vm.EventStatus == "Start") {
                eevent.Status = EventStatus.Started;
            }

            eevent.Start = DateTimeOffset.ParseExact($"{vm.StartDateTime}", "MM/dd/yyyy HH:mm", CultureInfo.InvariantCulture).UtcDateTime;
            eevent.End = DateTimeOffset.ParseExact($"{vm.EndDateTime}", "MM/dd/yyyy HH:mm", CultureInfo.InvariantCulture).UtcDateTime;
            eevent.FeedbackDate = DateTimeOffset.ParseExact($"{vm.FeedbackDateTime}", "MM/dd/yyyy HH:mm", CultureInfo.InvariantCulture).UtcDateTime;

            eevent.ImageUrl = await _cloudinary.UploadAsync(id, id, id, vm.EventImage);

            if (vm.HasTickets)
            {
                eevent.HasTickets = true;
                eevent.Tickets = new List<Item>();
                foreach (var oneItem in vm.EventItems)
                {
                    Item item = new Item();
                    item.Name = oneItem.Name;
                    item.Price = oneItem.TicketPrice.Value;
                    item.NumberOfItems = oneItem.NumberOfTickets.Value;
                    item.Description = oneItem.Description;
                    item.MaxReserveHoldDays = oneItem.MaxReserveHoldDays;
                    item.MaxReservablePerOrder = oneItem.MaxReservablePerOrder;
                    item.IsReservable = oneItem.IsReservable;

                    item.StartDate = DateTimeOffset.ParseExact($"{oneItem.StartDateTime}", "MM/dd/yyyy HH:mm", CultureInfo.InvariantCulture).UtcDateTime;
                    item.EndDate = DateTimeOffset.ParseExact($"{oneItem.EndDateTime}", "MM/dd/yyyy HH:mm", CultureInfo.InvariantCulture).UtcDateTime;

                    _ctx.Items.Add(item);
                    eevent.Tickets.Add(item);
                }

                //e.NumberOfTickets = vm.NumberOfTickets.Value;
                //e.TicketPrice = vm.TicketPrice.Value;
            }
            else
            {
                eevent.HasTickets = false;
                //e.NumberOfTickets = -1;
                //e.TicketPrice = -1;
            }

            bot.Events.Add(eevent);

            await _ctx.SaveChangesAsync();

            return RedirectToAction(nameof(Details), new { id = bot.Id });
        }

        

        [HttpGet]
        public async Task<IActionResult> PublishBot(string id)
        {
            CompanyAdmin companyAdmin = await this.GetCompanyAdminAsync(_userManager, User);
            Bot bot = await _ctx.Bots
                .Include(b => b.Plan)
                .SingleOrDefaultAsync(b => b.Id == id && b.CompanyId == companyAdmin.CompanyId);

            return View(bot);
        }
        [HttpPost]
        public async Task<IActionResult> EditBotEvent(string id, EventViewModel vm)
        {
            if (!ModelState.IsValid)
            {
                vm.BotId = id;
                return View(vm);
            }
            // Hardcore to GRCbot for adding events to bot
            CompanyAdmin companyAdmin = await this.GetCompanyAdminAsync(_userManager, User);
            Event eevent = await _ctx.Events.Include(b => b.Tickets).SingleOrDefaultAsync(e => e.Id == id);

            if (eevent == null)
                return RedirectToAction(nameof(Bots));

            eevent.Name = vm.Name;
            eevent.Description = vm.Description;
            eevent.Location = vm.Location;
            eevent.Start = DateTimeOffset.ParseExact($"{vm.StartDateTime}", "MM/dd/yyyy HH:mm", CultureInfo.InvariantCulture).UtcDateTime;
            eevent.End = DateTimeOffset.ParseExact($"{vm.EndDateTime}", "MM/dd/yyyy HH:mm", CultureInfo.InvariantCulture).UtcDateTime;
            eevent.FeedbackDate = DateTimeOffset.ParseExact($"{vm.FeedbackDateTime}", "MM/dd/yyyy HH:mm", CultureInfo.InvariantCulture).UtcDateTime;

            if (vm.HasTickets)
            {
                eevent.HasTickets = true;
                if (eevent.Tickets.Count == 0)
                {
                    eevent.Tickets = new List<Item>();
                    foreach (var oneItem in vm.EventItems)
                    {
                        Item item = new Item();
                        item.Name = oneItem.Name;
                        item.Price = oneItem.TicketPrice.Value;
                        item.NumberOfItems = oneItem.NumberOfTickets.Value;
                        item.Description = oneItem.Description;
                        item.MaxReserveHoldDays = oneItem.MaxReserveHoldDays;
                        item.MaxReservablePerOrder = oneItem.MaxReservablePerOrder;
                        item.IsReservable = oneItem.IsReservable;

                        item.StartDate = DateTimeOffset.ParseExact($"{oneItem.StartDateTime}", "MM/dd/yyyy HH:mm", CultureInfo.InvariantCulture).UtcDateTime;
                        item.EndDate = DateTimeOffset.ParseExact($"{oneItem.EndDateTime}", "MM/dd/yyyy HH:mm", CultureInfo.InvariantCulture).UtcDateTime;
                        _ctx.Items.Add(item);
                        eevent.Tickets.Add(item);
                    }
                }

                //e.NumberOfTickets = vm.NumberOfTickets.Value;
                //e.TicketPrice = vm.TicketPrice.Value;
            }
            else
            {
                eevent.HasTickets = false;
                if (eevent.Tickets.Count != 0)
                {
                    foreach (var oneItem in eevent.Tickets)
                    {
                        oneItem.NumberOfItems = 0;
                        _ctx.Items.Update(oneItem);
                    }
                    //e.NumberOfTickets = -1;
                    //e.TicketPrice = -1;
                }
            }

            await _ctx.SaveChangesAsync();

            return RedirectToAction(nameof(EditBotEvent), new { id = id });
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> PublishBot(string id, PublishBotViewModel vm)
        {
            CompanyAdmin companyAdmin = await this.GetCompanyAdminAsync(_userManager, User);
            Bot bot = await _ctx.Bots.SingleOrDefaultAsync(b => b.Id == id && b.CompanyId == companyAdmin.CompanyId);

            if (bot == null)
                return RedirectToAction(nameof(Bots));

            // Set the tokens/keys
            bot.TelegramApiToken = vm.TelegramApiToken;
            bot.FacebookPageId = vm.FacebookPageId;
            bot.FacebookAppId = vm.FacebookAppId;
            bot.FacebookAppSecret = vm.FacebookAppSecret;
            bot.FacebookPageAccessToken = vm.FacebookPageAccessToken;

            // set the bot to be pending deployment
            bot.Status = BotStatus.Processing;
            bot._DeployRequestedOn = DateTime.UtcNow;

            // email to some1 to alert publish request
            _emailSender.SendEmailAsync("ian@nyxintelligence.com", $"Bot publish request for {id}", "<p>details of bots</p><a>admin web site link</a>");

            // email admin that a bot publish request happened for this bot
            _emailSender.SendEmailAsync(companyAdmin.Email, $"A publish request for {bot.Name} was made", "<p>basic details of bots</p><a>link to bot details page</a>");

            await _ctx.SaveChangesAsync();

            TempData["Toast"] = $"Successfully request publish for {bot.Id}";
            TempData["ToastType"] = "is-success";
            return RedirectToAction(nameof(Details), new { id = id });
        }
        
        [HttpGet]
        public IActionResult RegisterCustomerSupport()
        {
            return View();
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> RegisterCustomerSupport(RegisterCustomerSupportViewModel vm)
        {
            TempData["ToastType"] = "is-danger";
            if (!ModelState.IsValid)
            {
                ModelErrorCollection errors = GetModelErrors();
                TempData["Toast"] = $"{errors.First().ErrorMessage}";
                return View(vm);
            }
            
            if (vm.Password != vm.ConfirmPassword)
            {
                TempData["Toast"] = $"The confirmation password does not match";
                return View(vm);
            }
            
            CompanyAdmin companyAdmin = await this.GetCompanyAdminAsync(_userManager, User);
            await _ctx.Entry(companyAdmin).Reference(ca => ca.Company).LoadAsync();
            await _ctx.Entry(companyAdmin.Company).Collection(c => c.CustomerSupports).LoadAsync();

            CompanyCustomerSupport user = new CompanyCustomerSupport
            {
                UserName = vm.Email,
                Email = vm.Email,
                FirstName = vm.FirstName,
                LastName = vm.LastName,
                //DateOfBirth = DateTime.ParseExact(vm.DOB, "dd/MM/yyyy", CultureInfo.InvariantCulture),
                _Active = true,
                _CreatedOn = DateTime.UtcNow
            };
            
            IdentityResult result = await _userManager.CreateAsync(user, vm.Password);
            if (result.Succeeded)
            {
                // send confirmation email
                string code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                string callbackUrl = Url.Action(nameof(AccountController.ConfirmEmail),
                    "Account",
                    new { userId = user.Id, code = code },
                    protocol: HttpContext.Request.Scheme);
                await _emailSender.SendEmailAsync(user.Email, "Confirm your account",
                        $"Please confirm your account by clicking this link: <a href=\"{callbackUrl}\">link</a>");

                // register firebase account
                string randomFirebasePassword = Utils.RandomGenerator.GetStringAlphanumeric(32);
                // if there was an excpetion then firebase api probably changed
                EmailSignUpResponse fbRes = await _firebaseService.EmailSignUpAsync(user.Email, randomFirebasePassword);
                user._FirebaseRefreshToken = fbRes.RefreshToken;

                user.ProfileImageUrl = await _cloudinary.UploadAsync(AccountController.cloudinaryFolder, user.Email, user.Email, vm.ProfileImg);
                user.CompanyId = companyAdmin.CompanyId;
                companyAdmin.Company.CustomerSupports.Add(user);
                await _ctx.SaveChangesAsync();
                await _userManager.AddToRoleAsync(user, AccountController.companyCustomerSupportRole);
                _logger.LogInformation(3, "User created a new account with password.");

                TempData["Toast"] = $"Successfully created the {vm.FirstName} {vm.LastName}.";
                TempData["ToastType"] = "is-success";
                return RedirectToAction(nameof(CustomerSupports));
            }

            return View(vm);
        }
        
        [HttpGet]
        public IActionResult CustomerSupports()
        {
            return View();
        }
        
        [HttpGet]
        public async Task<IActionResult> ViewCustomerSupport(string id)
        {
            CompanyAdmin companyAdmin = await this.GetCompanyAdminAsync(_userManager, User);
            CompanyCustomerSupport customerSupport = await _ctx.CompanyCustomerSupports
                .SingleOrDefaultAsync(ccs => ccs.CompanyId == companyAdmin.CompanyId);
            
            return View(customerSupport);
        }
        
        [HttpGet]
        public async Task<IActionResult> EditSupportingBots(string id)
        {
            CompanyAdmin companyAdmin = await this.GetCompanyAdminAsync(_userManager, User);
            CompanyCustomerSupport customerSupport = await _ctx.CompanyCustomerSupports
                .SingleOrDefaultAsync(ccs => ccs.CompanyId == companyAdmin.CompanyId
                    && ccs.Id == id);
                
            return View(customerSupport);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditSupportingBots(string id, List<SupportingBotViewModel> vms)
        {
            CompanyAdmin companyAdmin = await this.GetCompanyAdminAsync(_userManager, User);
            CompanyCustomerSupport customerSupport = await _ctx.CompanyCustomerSupports
                .Include(ccs => ccs.SupportingBots)
                .SingleOrDefaultAsync(ccs => ccs.CompanyId == companyAdmin.CompanyId
                    && ccs.Id == id);


            List<BotCustomerSupport> currentBotCustomerSupports = await _ctx.BotCustomerSupports
                .Where(bcs => bcs.CustomerSupportId == customerSupport.Id)
                .ToListAsync();

            List<Task> firebaseTasks = new List<Task>();
            
            foreach (SupportingBotViewModel vm in vms)
            {
                BotCustomerSupport botCustomerSupport = currentBotCustomerSupports
                    .SingleOrDefault(bcs => bcs.BotId == vm.Id);
                if (vm.IsSupported)
                {
                    firebaseTasks.Add(_firebaseService.SetWhiteListAsync(customerSupport, vm.Id, true));
                    if (botCustomerSupport == null)
                    {

                        customerSupport.SupportingBots.Add(new BotCustomerSupport
                        {
                            BotId = vm.Id,
                            CustomerSupportId = customerSupport.Id
                        });
                    }
                }
            }

            for (int i = currentBotCustomerSupports.Count() - 1; i >= 0; i--)
            {
                BotCustomerSupport bcs = currentBotCustomerSupports[i];
                if (!vms.Any(vm => vm.Id == bcs.BotId))
                {
                    firebaseTasks.Add(_firebaseService.SetWhiteListAsync(customerSupport, bcs.BotId, false));
                    _ctx.BotCustomerSupports.Remove(bcs);
                }
            }
            
            await _ctx.SaveChangesAsync();
            
            foreach (Task task in firebaseTasks)
            {
                await task;
            }
                
            return RedirectToAction(nameof(ViewCustomerSupport), new { id = customerSupport.Id });
        }
        
        [HttpPost]
        public async Task<IActionResult> DisableCustomerSupport(string id)
        {
            CompanyAdmin companyAdmin = await this.GetCompanyAdminAsync(_userManager, User);
            CompanyCustomerSupport customerSupport = await _ctx.CompanyCustomerSupports
                .SingleOrDefaultAsync(ccs => ccs.CompanyId == companyAdmin.CompanyId
                    && ccs.Id == id);

            customerSupport._Active = !customerSupport._Active;
            
            await _ctx.SaveChangesAsync();

            TempData["Toast"] = $"Disabled {customerSupport.FirstName} {customerSupport.LastName}";
            return RedirectToAction(nameof(ViewCustomerSupport), new { id = id });
        }
        
        [HttpPost]
        public async Task<IActionResult> ResetCustomerSupportPassword(string id)
        {
            // check if id is under this companyAdmin's company
            CompanyAdmin companyAdmin = await this.GetCompanyAdminAsync(_userManager, User);
            NyxCustomer user = await _userManager.FindByIdAsync(id);
            
            if (user == null)
            {
                return RedirectToAction(nameof(Index));
            }
            
            // generate new random password
            string newPasswword = RandomGenerator.GetStringAlphanumeric(16);

            // change to new password
            string token = await _userManager.GeneratePasswordResetTokenAsync(user);
            await _userManager.ResetPasswordAsync(user, token, newPasswword);

            // email new password to customerSupport's email
            await _emailSender.SendEmailAsync(user.Email, "Your administrator has reset your password", $"Your new password is: {newPasswword}");

            // redirect to customerSupports page with 
            // toast saying password reset email has been sent
            return RedirectToAction(nameof(ViewCustomerSupport), new { id = user.Id });
        }

        [HttpGet]
        public IActionResult ViewVolunteers()
        {
            return View();
        }

        [HttpGet]
        public IActionResult ViewFeedback()
        {
            return View();
        }

        [HttpGet]
        public IActionResult ViewGeneralFeedback()
        {
            return View();
        }

        private ModelErrorCollection GetModelErrors()
        {
            return ModelState.Values
                    .Select(v => v.Errors)
                    .Aggregate(new ModelErrorCollection(), (allErrors, vErrors) =>
                    {
                        foreach (ModelError modelError in vErrors)
                        {
                            allErrors.Add(modelError);
                        }
                        return allErrors;
                    });
        }

        [HttpGet]
        public IActionResult EditQnA()
        {
            return View();
        }
    }
}
