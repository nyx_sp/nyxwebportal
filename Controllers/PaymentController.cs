﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NyxDatabase.Data;
using NyxDatabase.Models;
using NyxWebPortal.Models.Payment;
using NyxWebPortal.Services;
using NyxWebPortal.Services.Firebase;
using NyxWebPortal.Utils;

namespace NyxWebPortal.Controllers
{
    public class PaymentController : Controller
    {
        private readonly NyxDbContext _ctx;
        private readonly UserManager<NyxCustomer> _userManager;
        private readonly SignInManager<NyxCustomer> _signInManager;
        private readonly IEmailSender _emailSender;
        private readonly ISmsSender _smsSender;
        private readonly ILogger _logger;
        private readonly CloudinaryService _cloudinary;
        private readonly FirebaseService _firebaseService;

        private const string cloudinaryFolder = "NyxCustomers";

        public PaymentController(
            NyxDbContext ctx,
            UserManager<NyxCustomer> userManager,
            SignInManager<NyxCustomer> signInManager,
            IEmailSender emailSender,
            ISmsSender smsSender,
            ILoggerFactory loggerFactory,
            CloudinaryService cloudinary,
            FirebaseService firebaseService)
        {
            _ctx = ctx;
            _userManager = userManager;
            _signInManager = signInManager;
            _emailSender = emailSender;
            _smsSender = smsSender;
            _logger = loggerFactory.CreateLogger<AccountController>();
            _cloudinary = cloudinary;
            _firebaseService = firebaseService;
        }
        
        [HttpGet]
        public async Task<IActionResult> BuyEventTickets(string id, [FromQuery] string botUserId)
        {
            Event eevent = await _ctx.Events
                .Include(e => e.Bot)
                    .ThenInclude(b => b.Company)
                .Where(e => e.Id == id)
                .FirstOrDefaultAsync();
            if (eevent.Status == EventStatus.Started)
            {
                return View(new PayViewModel
                {
                    BotId = eevent.BotId,
                    BotName = eevent.Bot.Name,
                    CompanyName = eevent.Bot.Company.Name,
                    EventId = eevent.Id,
                    BotUserId = botUserId
                });
            }
            else {
                return RedirectToAction("Index", "Home");
            }
        }
        
        [HttpPost]
        public async Task<IActionResult> BuyEventTickets(BuyEventTicketsViewModel vm, [FromQuery] string botUserId)
        {
            try
            {
                Event eevent = await _ctx.Events
                    .Include(e => e.Bot)
                        .ThenInclude(b => b.Company)
                            .ThenInclude(c => c.Admin)
                    .Where(e => e.Id == vm.EventId)
                    .SingleOrDefaultAsync();

                decimal total = 0;
                foreach (ItemViewModel itemvm in vm.Items)
                {
                    if (itemvm.Quantity > 0)
                    {
                        Item item = await _ctx.Items.SingleOrDefaultAsync(i => i.Id == itemvm.Id);
                        total += (itemvm.Quantity * item.Price);
                    }
                }
                total = total * 100;
                int cent = Convert.ToInt32(total);
                string result = "";
                result = await StripeServices.StripePayment(eevent.Bot.Company.Admin.StripeConnectId, vm.StripeTransactionId, cent, "TestPayment for testing1234567890");
                JObject jObject = JObject.Parse(result);
                var ErrorCheck = jObject["error"];
                if (ErrorCheck == null)
                {
                    ModelState.Clear();
                    var StripeReceiptId = jObject["id"].ToString();

                    string msg = "You have purchase: <br/>";
                    foreach (ItemViewModel itemvm in vm.Items)
                    {
                        if (itemvm.Quantity > 0)
                        {
                            Item item = await _ctx.Items.SingleOrDefaultAsync(i => i.Id == itemvm.Id);
                            string transactionId = Guid.NewGuid().ToString();
                            DateTimeOffset createdOn = DateTimeOffset.UtcNow;
                            BotTransaction botTransaction = new BotTransaction();
                            botTransaction.TransactionId = transactionId;
                            botTransaction.BotId = eevent.BotId;
                            botTransaction.CompanyId = eevent.Bot.CompanyId;
                            botTransaction.CompanyName = eevent.Bot.Company.Name;
                            botTransaction.AdminId = eevent.Bot.Company.Admin.Id;
                            botTransaction.AdminName = $"{eevent.Bot.Company.Admin.FirstName} {eevent.Bot.Company.Admin.LastName}";
                            botTransaction.Status = BotTransactionStatus.Completed;
                            botTransaction.StripeReceiptId = StripeReceiptId;
                            botTransaction.ItemId = item.Id;
                            botTransaction.ItemName = item.Name;
                            botTransaction.Quantity = itemvm.Quantity;
                            botTransaction.Amount = (itemvm.Quantity * item.Price);
                            _ctx.BotTransactions.Add(botTransaction);

                            msg += " " + itemvm.Quantity + " x " + item.Name + " <br/>";
                            msg += "Transaction ID: " + botTransaction.TransactionId + " <br/>";

                        }
                    }
                    msg += "Please show the ticket at the event! <br/>";
                    msg += "The event will be held on " + eevent.Start.UtcDateTime + " and ends on " + eevent.End.UtcDateTime;
                    await _ctx.SaveChangesAsync();
                    await _emailSender.SendCalenderInviteEmailAsync(vm.Email, "Ticket for " + eevent.Name, msg, eevent.Start, eevent.End, eevent.Bot.Company.Name, "");
                    return RedirectToAction(nameof(Bought));
                }
                else
                {
                    ModelState.Clear();
                    string msg = (string)ErrorCheck.SelectToken("message");
                    //Index("testtest", msg)            
                    TempData["ToastType"] = "is-danger";
                    TempData["Toast"] = $"{msg}";

                    return RedirectToAction(nameof(BuyEventTickets));

                }
            }
            catch (Exception err) {
                TempData["ToastType"] = "is-danger";
                TempData["Toast"] = $"Something went wrong! Please try again!";
                return RedirectToAction(nameof(BuyEventTickets));
            }
        }

        [HttpGet]
        public IActionResult Bought(string id)
        {
            return View();
        }

        [HttpGet]
        public IActionResult IFramePaymentDetails()
        {
            return View();
        }

    }
}