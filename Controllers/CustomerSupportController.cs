﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using NyxDatabase.Data;
using NyxWebPortal.Services.Firebase;
using NyxDatabase.Models;
using System.Net.Http;
using Newtonsoft.Json;
using System.Text;
using Microsoft.AspNetCore.Authorization;
using NyxWebPortal.Utils;
using Microsoft.EntityFrameworkCore;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace NyxWebPortal.Controllers
{

    [Authorize(Roles = AccountController.companyCustomerSupportRole+","+AccountController.companyAdminRole)]
    public class CustomerSupportController : Controller
    {
        private readonly NyxDbContext _ctx;
        private readonly UserManager<NyxCustomer> _userManager;
        private readonly FirebaseService _firebaseService;

        public CustomerSupportController(
            NyxDbContext ctx,
            UserManager<NyxCustomer> userManager,
            FirebaseService firebaseService)
        {
            _ctx = ctx;
            _userManager = userManager;
            _firebaseService = firebaseService;
        }
        
        public async Task<IActionResult> Index()
        {
            CompanyAdmin companyCustomerSupport = await this.GetCompanyAdminAsync(_userManager, User);
            List<Bot> bots = new List<Bot>();
                bots = await _ctx.Bots
                   .Where(b => b.CompanyId == companyCustomerSupport.CompanyId)
                   .ToListAsync();
            return View(bots);
        }
        
        public async Task<IActionResult> Chat()
        {
            NyxCustomer currentUser = await _userManager.GetUserAsync(User);
            string customToken = await _firebaseService.CustomTokenAsync(currentUser._FirebaseRefreshToken);

            @ViewData["customToken"] = customToken;
            ViewData["current_user"] = currentUser.Email;

            return View();
        }
        public ActionResult EditQuestion()
        {
            return View();
        }
        public ActionResult EditSmallTalk()
        {
            return View();
        }

        [HttpGet("/[controller]/[action]/{botId}")]
        public async Task<IActionResult> getBotIntent(string botId)
        {
            HttpClient client = new HttpClient
            {
                BaseAddress = new Uri("https://nyxdata-v2.firebaseio.com/Bot/"),
            };
            try
            {
                var response = await client.GetAsync(botId + "/Intents.json");
                var result = response.Content.ReadAsStringAsync().Result;
                var jsonResult = JsonConvert.DeserializeObject(result);
                return new JsonResult(jsonResult);
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }
        [HttpPut("/[controller]/[action]/{botId}")]
        // you guys can change the JArray to models as soon as you guys figure out the model thing in the 
        // bot repo problem
        public async Task<IActionResult> updateBotIntent(string botId, [FromBody]Newtonsoft.Json.Linq.JArray value)
        {
            var resrponds = Request;
            HttpClient client = new HttpClient
            {
                BaseAddress = new Uri("https://nyxdata-v2.firebaseio.com/Bot/"),
            };
            try
            {
                string data = JsonConvert.SerializeObject(value);
                var content = new StringContent(data, Encoding.UTF8, "application/json");
                var response = await client.PutAsync(botId + "/Intents.json", content);
                bool status = response.IsSuccessStatusCode;
                if (status)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpGet("/[controller]/[action]/{botId}")]
        public async Task<IActionResult> getSmallTalk(string botId)
        {
            HttpClient client = new HttpClient
            {
                BaseAddress = new Uri("https://nyxdata-v2.firebaseio.com/Bot/"),
            };
            try
            {
                var response = await client.GetAsync(botId + "/SmallTalk.json");
                var result = response.Content.ReadAsStringAsync().Result;
                var jsonResult = JsonConvert.DeserializeObject(result);
                return new JsonResult(jsonResult);
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpPut("/[controller]/[action]/{botId}")]
        // you guys can change the JArray to models as soon as you guys figure out the model thing in the 
        // bot repo problem
        public async Task<IActionResult> updateSmallTalk(string botId, [FromBody]Newtonsoft.Json.Linq.JArray value)
        {
            var resrponds = Request;
            HttpClient client = new HttpClient
            {
                BaseAddress = new Uri("https://nyxdata-v2.firebaseio.com/Bot/"),
            };
            try
            {
                string data = JsonConvert.SerializeObject(value);
                var content = new StringContent(data, Encoding.UTF8, "application/json");
                var response = await client.PutAsync(botId + "/SmallTalk.json", content);
                bool status = response.IsSuccessStatusCode;
                if (status)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }
    }
}
