﻿using System;
using System.Linq;
using System.Security.Cryptography;

namespace NyxWebPortal.Utils
{
    public class RandomGenerator
    {
        private static RandomNumberGenerator rng = RandomNumberGenerator.Create();
        private const string base80 = "abcdefghijklmnopqrstuvwyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*():;,./<>?";
        private const string base62 = "abcdefghijklmnopqrstuvwyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        
        public static string GetString(int length)
        {
            return new string(Enumerable.Repeat(base80, length)
              .Select(s => s[GetInt(base80.Length)]).ToArray());
        }

        public static string GetStringAlphanumeric(int length)
        {
            return new string(Enumerable.Repeat(base62, length)
              .Select(s => s[GetInt(base62.Length)]).ToArray());
        }

        public static int GetInt(int max)
        {
            if (max < 1)
            {
                throw new ArgumentException("max cannot be less than 1");
            }

            int randomInt = 0;
            byte[] maxIntMask = GetMaskFromInt(max);
            do
            {
                randomInt = GetInt(maxIntMask);
            } while (randomInt >= max);

            return randomInt;
        }
        
        public static int GetInt(byte[] mask)
        {
            byte[] bytes = new byte[4];
            rng.GetBytes(bytes);
            for (int i = 0; i < 4; i++)
            {
                bytes[i] &= mask[i];
            }
            int randomInt = BitConverter.ToInt32(bytes, 0);
            return randomInt;
        }
        
        private static byte[] GetMaskFromInt(int biggestInt)
        {
            byte[] bytes = BitConverter.GetBytes(biggestInt);
            bool foundFirst1Bit = false;
            // Little Endian
            for (int i = 31; i >= 0; i--)
            {
                int byteIndex = i / 8;
                int bitInByteIndex = i % 8;
                byte mask = (byte)(1 << bitInByteIndex);

                if (foundFirst1Bit)
                {
                    bytes[byteIndex] |= mask;
                    continue;
                }
                
                foundFirst1Bit = (bytes[byteIndex] & mask) != 0;
            }

            return bytes;
        }
    }
}
