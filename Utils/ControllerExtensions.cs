﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using NyxDatabase.Models;

namespace NyxWebPortal.Utils
{
    // You may need to install the Microsoft.AspNetCore.Http.Abstractions package into your project
    public static class ControllerExtensions
    {
        public static async Task<NyxCustomer> GetNyxCustomerAsync(this Controller controller, UserManager<NyxCustomer> userManager, ClaimsPrincipal user)
        {
            return await userManager.GetUserAsync(user);
        }
        
        public static async Task<CompanyAdmin> GetCompanyAdminAsync(this Controller controller, UserManager<NyxCustomer> userManager, ClaimsPrincipal user)
        {
            return (CompanyAdmin)await GetNyxCustomerAsync(controller, userManager, user);
        }
        
        public static async Task<CompanyCustomerSupport> GetCompanyCustomerSupportAsync(this Controller controller, UserManager<NyxCustomer> userManager, ClaimsPrincipal user)
        {
            return (CompanyCustomerSupport)await GetNyxCustomerAsync(controller, userManager, user);
        }
    }
}
