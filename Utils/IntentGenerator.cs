﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using NyxDatabase.Models;

namespace NyxWebPortal.Utils
{
    public class IntentGenerator
    {
        private List<EntityResponse> GenerateEntityResponses(string message, string[] texts)
        {
            List<EntityResponse> result = new List<EntityResponse>();
            foreach (string text in texts)
            {
                result.Add(new EntityResponse(
                    message,
                    text
                ));
            }
            return result;
        }
    }
    
    public class Intent
    {
        public int Id { get; set; }
        public List<Entity> entities { get; set; }
        public List<EntityResponse> entityResponse { get; set; }
        public string intent { get; set; }

        public string BotId { get; set; }
    }
    
    public class Entity
    {
        public int Id { get; set; }
        public string entity { get; set; }
    }
    
    public class EntityResponse
    {
        public int Id { get; set; }

        [JsonConstructor]
        public EntityResponse(string contType, string entity, string message, string text)
        {
            this.contType = contType;
            this.entity = entity;
            this.message = message;
            this.text = text;
        }
        public EntityResponse(string message, string text)
        {
            this.contType = "text";
            this.entity = "object";
            this.message = message;
            this.text = text;
        }
        public string contType { get; set; }
        public string entity { get; set; }
        public string message { get; set; }
        public string text { get; set; }
    }
}
