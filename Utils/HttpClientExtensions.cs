﻿using System;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;

namespace NyxWebPortal.Utils
{
    public static class HttpClientExtensions
    {
       public static async Task<HttpResponseMessage> PatchAsync(this HttpClient client, string requestUrl, HttpContent iContent)
       {
           var method = new HttpMethod("PATCH");
           var request = new HttpRequestMessage(method, requestUrl)
           {
               Content = iContent
           };
    
           HttpResponseMessage response = new HttpResponseMessage();
           try
           {
               response = await client.SendAsync(request);
           }
           catch (TaskCanceledException e)
           {
               Debug.WriteLine("ERROR: " + e.ToString());
           }
    
           return response;
       }
    }
}
