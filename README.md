# Nyx Web Portal

## Stack
- AspNet core 2.0.0
- Node 9.8.0 (for tools)
- VueJs
- Buefy

## Setup
1. Get environment variables
2. Dotnet core cli or visual studio
3. Install gulp
4. Get connection to the private nuget feed
