﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using NyxDatabase.Data;
using NyxDatabase.Models;

namespace NyxWebPortal
{
    public class SeedDB
    {
        public static async Task InitializeAsync(IServiceProvider services) {
            using (NyxDbContext ctx = services.GetService(typeof(NyxDbContext)) as NyxDbContext)
            using (RoleManager<IdentityRole> roleManager = services.GetService(typeof(RoleManager<IdentityRole>)) as RoleManager<IdentityRole>)
            using (UserManager<NyxCustomer> userManager = services.GetService(typeof(UserManager<NyxCustomer>)) as UserManager<NyxCustomer>)
            {

                // Add 'companyAdmin' role
                IdentityRole companyAdminRole = await roleManager.FindByNameAsync("CompanyAdmin");
                if (companyAdminRole == null)
                {
                    companyAdminRole = new IdentityRole("CompanyAdmin");
                    await roleManager.CreateAsync(companyAdminRole);
                }

                // Add 'customerSupport' role
                IdentityRole customerSupportRole = await roleManager.FindByNameAsync("CompanyCustomerSupport");
                if (customerSupportRole == null)
                {
                    customerSupportRole = new IdentityRole("CompanyCustomerSupport");
                    await roleManager.CreateAsync(customerSupportRole);
                }

                // seed countries
                if (ctx.Countries.Count() < 1)
                {
                    ctx.Countries.Add(new Country { Id = "Afghanistan" });
                    ctx.Countries.Add(new Country { Id = "Åland Islands" });
                    ctx.Countries.Add(new Country { Id = "Albania" });
                    ctx.Countries.Add(new Country { Id = "Algeria" });
                    ctx.Countries.Add(new Country { Id = "American Samoa" });
                    ctx.Countries.Add(new Country { Id = "AndorrA" });
                    ctx.Countries.Add(new Country { Id = "Angola" });
                    ctx.Countries.Add(new Country { Id = "Anguilla" });
                    ctx.Countries.Add(new Country { Id = "Antarctica" });
                    ctx.Countries.Add(new Country { Id = "Antigua and Barbuda" });
                    ctx.Countries.Add(new Country { Id = "Argentina" });
                    ctx.Countries.Add(new Country { Id = "Armenia" });
                    ctx.Countries.Add(new Country { Id = "Aruba" });
                    ctx.Countries.Add(new Country { Id = "Australia" });
                    ctx.Countries.Add(new Country { Id = "Austria" });
                    ctx.Countries.Add(new Country { Id = "Azerbaijan" });
                    ctx.Countries.Add(new Country { Id = "Bahamas" });
                    ctx.Countries.Add(new Country { Id = "Bahrain" });
                    ctx.Countries.Add(new Country { Id = "Bangladesh" });
                    ctx.Countries.Add(new Country { Id = "Barbados" });
                    ctx.Countries.Add(new Country { Id = "Belarus" });
                    ctx.Countries.Add(new Country { Id = "Belgium" });
                    ctx.Countries.Add(new Country { Id = "Belize" });
                    ctx.Countries.Add(new Country { Id = "Benin" });
                    ctx.Countries.Add(new Country { Id = "Bermuda" });
                    ctx.Countries.Add(new Country { Id = "Bhutan" });
                    ctx.Countries.Add(new Country { Id = "Bolivia" });
                    ctx.Countries.Add(new Country { Id = "Bosnia and Herzegovina" });
                    ctx.Countries.Add(new Country { Id = "Botswana" });
                    ctx.Countries.Add(new Country { Id = "Bouvet Island" });
                    ctx.Countries.Add(new Country { Id = "Brazil" });
                    ctx.Countries.Add(new Country { Id = "British Indian Ocean Territory" });
                    ctx.Countries.Add(new Country { Id = "Brunei Darussalam" });
                    ctx.Countries.Add(new Country { Id = "Bulgaria" });
                    ctx.Countries.Add(new Country { Id = "Burkina Faso" });
                    ctx.Countries.Add(new Country { Id = "Burundi" });
                    ctx.Countries.Add(new Country { Id = "Cambodia" });
                    ctx.Countries.Add(new Country { Id = "Cameroon" });
                    ctx.Countries.Add(new Country { Id = "Canada" });
                    ctx.Countries.Add(new Country { Id = "Cape Verde" });
                    ctx.Countries.Add(new Country { Id = "Cayman Islands" });
                    ctx.Countries.Add(new Country { Id = "Central African Republic" });
                    ctx.Countries.Add(new Country { Id = "Chad" });
                    ctx.Countries.Add(new Country { Id = "Chile" });
                    ctx.Countries.Add(new Country { Id = "China" });
                    ctx.Countries.Add(new Country { Id = "Christmas Island" });
                    ctx.Countries.Add(new Country { Id = "Cocos (Keeling) Islands" });
                    ctx.Countries.Add(new Country { Id = "Colombia" });
                    ctx.Countries.Add(new Country { Id = "Comoros" });
                    ctx.Countries.Add(new Country { Id = "Congo" });
                    ctx.Countries.Add(new Country { Id = "Congo, The Democratic Republic of the" });
                    ctx.Countries.Add(new Country { Id = "Cook Islands" });
                    ctx.Countries.Add(new Country { Id = "Costa Rica" });
                    ctx.Countries.Add(new Country { Id = "Cote D\"Ivoire" });
                    ctx.Countries.Add(new Country { Id = "Croatia" });
                    ctx.Countries.Add(new Country { Id = "Cuba" });
                    ctx.Countries.Add(new Country { Id = "Cyprus" });
                    ctx.Countries.Add(new Country { Id = "Czech Republic" });
                    ctx.Countries.Add(new Country { Id = "Denmark" });
                    ctx.Countries.Add(new Country { Id = "Djibouti" });
                    ctx.Countries.Add(new Country { Id = "Dominica" });
                    ctx.Countries.Add(new Country { Id = "Dominican Republic" });
                    ctx.Countries.Add(new Country { Id = "Ecuador" });
                    ctx.Countries.Add(new Country { Id = "Egypt" });
                    ctx.Countries.Add(new Country { Id = "El Salvador" });
                    ctx.Countries.Add(new Country { Id = "Equatorial Guinea" });
                    ctx.Countries.Add(new Country { Id = "Eritrea" });
                    ctx.Countries.Add(new Country { Id = "Estonia" });
                    ctx.Countries.Add(new Country { Id = "Ethiopia" });
                    ctx.Countries.Add(new Country { Id = "Falkland Islands (Malvinas)" });
                    ctx.Countries.Add(new Country { Id = "Faroe Islands" });
                    ctx.Countries.Add(new Country { Id = "Fiji" });
                    ctx.Countries.Add(new Country { Id = "Finland" });
                    ctx.Countries.Add(new Country { Id = "France" });
                    ctx.Countries.Add(new Country { Id = "French Guiana" });
                    ctx.Countries.Add(new Country { Id = "French Polynesia" });
                    ctx.Countries.Add(new Country { Id = "French Southern Territories" });
                    ctx.Countries.Add(new Country { Id = "Gabon" });
                    ctx.Countries.Add(new Country { Id = "Gambia" });
                    ctx.Countries.Add(new Country { Id = "Georgia" });
                    ctx.Countries.Add(new Country { Id = "Germany" });
                    ctx.Countries.Add(new Country { Id = "Ghana" });
                    ctx.Countries.Add(new Country { Id = "Gibraltar" });
                    ctx.Countries.Add(new Country { Id = "Greece" });
                    ctx.Countries.Add(new Country { Id = "Greenland" });
                    ctx.Countries.Add(new Country { Id = "Grenada" });
                    ctx.Countries.Add(new Country { Id = "Guadeloupe" });
                    ctx.Countries.Add(new Country { Id = "Guam" });
                    ctx.Countries.Add(new Country { Id = "Guatemala" });
                    ctx.Countries.Add(new Country { Id = "Guernsey" });
                    ctx.Countries.Add(new Country { Id = "Guinea" });
                    ctx.Countries.Add(new Country { Id = "Guinea-Bissau" });
                    ctx.Countries.Add(new Country { Id = "Guyana" });
                    ctx.Countries.Add(new Country { Id = "Haiti" });
                    ctx.Countries.Add(new Country { Id = "Heard Island and Mcdonald Islands" });
                    ctx.Countries.Add(new Country { Id = "Holy See (Vatican City State)" });
                    ctx.Countries.Add(new Country { Id = "Honduras" });
                    ctx.Countries.Add(new Country { Id = "Hong Kong" });
                    ctx.Countries.Add(new Country { Id = "Hungary" });
                    ctx.Countries.Add(new Country { Id = "Iceland" });
                    ctx.Countries.Add(new Country { Id = "India" });
                    ctx.Countries.Add(new Country { Id = "Indonesia" });
                    ctx.Countries.Add(new Country { Id = "Iran, Islamic Republic Of" });
                    ctx.Countries.Add(new Country { Id = "Iraq" });
                    ctx.Countries.Add(new Country { Id = "Ireland" });
                    ctx.Countries.Add(new Country { Id = "Isle of Man" });
                    ctx.Countries.Add(new Country { Id = "Israel" });
                    ctx.Countries.Add(new Country { Id = "Italy" });
                    ctx.Countries.Add(new Country { Id = "Jamaica" });
                    ctx.Countries.Add(new Country { Id = "Japan" });
                    ctx.Countries.Add(new Country { Id = "Jersey" });
                    ctx.Countries.Add(new Country { Id = "Jordan" });
                    ctx.Countries.Add(new Country { Id = "Kazakhstan" });
                    ctx.Countries.Add(new Country { Id = "Kenya" });
                    ctx.Countries.Add(new Country { Id = "Kiribati" });
                    ctx.Countries.Add(new Country { Id = "Korea, Democratic People\"S Republic of" });
                    ctx.Countries.Add(new Country { Id = "Korea, Republic of" });
                    ctx.Countries.Add(new Country { Id = "Kuwait" });
                    ctx.Countries.Add(new Country { Id = "Kyrgyzstan" });
                    ctx.Countries.Add(new Country { Id = "Lao People\"S Democratic Republic" });
                    ctx.Countries.Add(new Country { Id = "Latvia" });
                    ctx.Countries.Add(new Country { Id = "Lebanon" });
                    ctx.Countries.Add(new Country { Id = "Lesotho" });
                    ctx.Countries.Add(new Country { Id = "Liberia" });
                    ctx.Countries.Add(new Country { Id = "Libyan Arab Jamahiriya" });
                    ctx.Countries.Add(new Country { Id = "Liechtenstein" });
                    ctx.Countries.Add(new Country { Id = "Lithuania" });
                    ctx.Countries.Add(new Country { Id = "Luxembourg" });
                    ctx.Countries.Add(new Country { Id = "Macao" });
                    ctx.Countries.Add(new Country { Id = "Macedonia, The Former Yugoslav Republic of" });
                    ctx.Countries.Add(new Country { Id = "Madagascar" });
                    ctx.Countries.Add(new Country { Id = "Malawi" });
                    ctx.Countries.Add(new Country { Id = "Malaysia" });
                    ctx.Countries.Add(new Country { Id = "Maldives" });
                    ctx.Countries.Add(new Country { Id = "Mali" });
                    ctx.Countries.Add(new Country { Id = "Malta" });
                    ctx.Countries.Add(new Country { Id = "Marshall Islands" });
                    ctx.Countries.Add(new Country { Id = "Martinique" });
                    ctx.Countries.Add(new Country { Id = "Mauritania" });
                    ctx.Countries.Add(new Country { Id = "Mauritius" });
                    ctx.Countries.Add(new Country { Id = "Mayotte" });
                    ctx.Countries.Add(new Country { Id = "Mexico" });
                    ctx.Countries.Add(new Country { Id = "Micronesia, Federated States of" });
                    ctx.Countries.Add(new Country { Id = "Moldova, Republic of" });
                    ctx.Countries.Add(new Country { Id = "Monaco" });
                    ctx.Countries.Add(new Country { Id = "Mongolia" });
                    ctx.Countries.Add(new Country { Id = "Montserrat" });
                    ctx.Countries.Add(new Country { Id = "Morocco" });
                    ctx.Countries.Add(new Country { Id = "Mozambique" });
                    ctx.Countries.Add(new Country { Id = "Myanmar" });
                    ctx.Countries.Add(new Country { Id = "Namibia" });
                    ctx.Countries.Add(new Country { Id = "Nauru" });
                    ctx.Countries.Add(new Country { Id = "Nepal" });
                    ctx.Countries.Add(new Country { Id = "Netherlands" });
                    ctx.Countries.Add(new Country { Id = "Netherlands Antilles" });
                    ctx.Countries.Add(new Country { Id = "New Caledonia" });
                    ctx.Countries.Add(new Country { Id = "New Zealand" });
                    ctx.Countries.Add(new Country { Id = "Nicaragua" });
                    ctx.Countries.Add(new Country { Id = "Niger" });
                    ctx.Countries.Add(new Country { Id = "Nigeria" });
                    ctx.Countries.Add(new Country { Id = "Niue" });
                    ctx.Countries.Add(new Country { Id = "Norfolk Island" });
                    ctx.Countries.Add(new Country { Id = "Northern Mariana Islands" });
                    ctx.Countries.Add(new Country { Id = "Norway" });
                    ctx.Countries.Add(new Country { Id = "Oman" });
                    ctx.Countries.Add(new Country { Id = "Pakistan" });
                    ctx.Countries.Add(new Country { Id = "Palau" });
                    ctx.Countries.Add(new Country { Id = "Palestinian Territory, Occupied" });
                    ctx.Countries.Add(new Country { Id = "Panama" });
                    ctx.Countries.Add(new Country { Id = "Papua New Guinea" });
                    ctx.Countries.Add(new Country { Id = "Paraguay" });
                    ctx.Countries.Add(new Country { Id = "Peru" });
                    ctx.Countries.Add(new Country { Id = "Philippines" });
                    ctx.Countries.Add(new Country { Id = "Pitcairn" });
                    ctx.Countries.Add(new Country { Id = "Poland" });
                    ctx.Countries.Add(new Country { Id = "Portugal" });
                    ctx.Countries.Add(new Country { Id = "Puerto Rico" });
                    ctx.Countries.Add(new Country { Id = "Qatar" });
                    ctx.Countries.Add(new Country { Id = "Reunion" });
                    ctx.Countries.Add(new Country { Id = "Romania" });
                    ctx.Countries.Add(new Country { Id = "Russian Federation" });
                    ctx.Countries.Add(new Country { Id = "RWANDA" });
                    ctx.Countries.Add(new Country { Id = "Saint Helena" });
                    ctx.Countries.Add(new Country { Id = "Saint Kitts and Nevis" });
                    ctx.Countries.Add(new Country { Id = "Saint Lucia" });
                    ctx.Countries.Add(new Country { Id = "Saint Pierre and Miquelon" });
                    ctx.Countries.Add(new Country { Id = "Saint Vincent and the Grenadines" });
                    ctx.Countries.Add(new Country { Id = "Samoa" });
                    ctx.Countries.Add(new Country { Id = "San Marino" });
                    ctx.Countries.Add(new Country { Id = "Sao Tome and Principe" });
                    ctx.Countries.Add(new Country { Id = "Saudi Arabia" });
                    ctx.Countries.Add(new Country { Id = "Senegal" });
                    ctx.Countries.Add(new Country { Id = "Serbia and Montenegro" });
                    ctx.Countries.Add(new Country { Id = "Seychelles" });
                    ctx.Countries.Add(new Country { Id = "Sierra Leone" });
                    ctx.Countries.Add(new Country { Id = "Singapore" });
                    ctx.Countries.Add(new Country { Id = "Slovakia" });
                    ctx.Countries.Add(new Country { Id = "Slovenia" });
                    ctx.Countries.Add(new Country { Id = "Solomon Islands" });
                    ctx.Countries.Add(new Country { Id = "Somalia" });
                    ctx.Countries.Add(new Country { Id = "South Africa" });
                    ctx.Countries.Add(new Country { Id = "South Georgia and the South Sandwich Islands" });
                    ctx.Countries.Add(new Country { Id = "Spain" });
                    ctx.Countries.Add(new Country { Id = "Sri Lanka" });
                    ctx.Countries.Add(new Country { Id = "Sudan" });
                    ctx.Countries.Add(new Country { Id = "Suriname" });
                    ctx.Countries.Add(new Country { Id = "Svalbard and Jan Mayen" });
                    ctx.Countries.Add(new Country { Id = "Swaziland" });
                    ctx.Countries.Add(new Country { Id = "Sweden" });
                    ctx.Countries.Add(new Country { Id = "Switzerland" });
                    ctx.Countries.Add(new Country { Id = "Syrian Arab Republic" });
                    ctx.Countries.Add(new Country { Id = "Taiwan, Province of China" });
                    ctx.Countries.Add(new Country { Id = "Tajikistan" });
                    ctx.Countries.Add(new Country { Id = "Tanzania, United Republic of" });
                    ctx.Countries.Add(new Country { Id = "Thailand" });
                    ctx.Countries.Add(new Country { Id = "Timor-Leste" });
                    ctx.Countries.Add(new Country { Id = "Togo" });
                    ctx.Countries.Add(new Country { Id = "Tokelau" });
                    ctx.Countries.Add(new Country { Id = "Tonga" });
                    ctx.Countries.Add(new Country { Id = "Trinidad and Tobago" });
                    ctx.Countries.Add(new Country { Id = "Tunisia" });
                    ctx.Countries.Add(new Country { Id = "Turkey" });
                    ctx.Countries.Add(new Country { Id = "Turkmenistan" });
                    ctx.Countries.Add(new Country { Id = "Turks and Caicos Islands" });
                    ctx.Countries.Add(new Country { Id = "Tuvalu" });
                    ctx.Countries.Add(new Country { Id = "Uganda" });
                    ctx.Countries.Add(new Country { Id = "Ukraine" });
                    ctx.Countries.Add(new Country { Id = "United Arab Emirates" });
                    ctx.Countries.Add(new Country { Id = "United Kingdom" });
                    ctx.Countries.Add(new Country { Id = "United States" });
                    ctx.Countries.Add(new Country { Id = "United States Minor Outlying Islands" });
                    ctx.Countries.Add(new Country { Id = "Uruguay" });
                    ctx.Countries.Add(new Country { Id = "Uzbekistan" });
                    ctx.Countries.Add(new Country { Id = "Vanuatu" });
                    ctx.Countries.Add(new Country { Id = "Venezuela" });
                    ctx.Countries.Add(new Country { Id = "Viet Nam" });
                    ctx.Countries.Add(new Country { Id = "Virgin Islands, British" });
                    ctx.Countries.Add(new Country { Id = "Virgin Islands, U.S." });
                    ctx.Countries.Add(new Country { Id = "Wallis and Futuna" });
                    ctx.Countries.Add(new Country { Id = "Western Sahara" });
                    ctx.Countries.Add(new Country { Id = "Yemen" });
                    ctx.Countries.Add(new Country { Id = "Zambia" });
                    ctx.Countries.Add(new Country { Id = "Zimbabwe" });

                    await ctx.SaveChangesAsync();
                }

                //if (ctx.ParkingOptions.Count() < 1)
                //{
                //    ctx.ParkingOptions.Add(new ParkingOption { Id = "Bike" });
                //    ctx.ParkingOptions.Add(new ParkingOption { Id = "Car" });
                //    await ctx.SaveChangesAsync();
                //}

                //if (ctx.DietOptions.Count() < 1)
                //{
                //    ctx.DietOptions.Add(new DietOption { Id = "Alcoholic" });
                //    ctx.DietOptions.Add(new DietOption { Id = "Halal" });
                //    ctx.DietOptions.Add(new DietOption { Id = "Vegan" });
                //    ctx.DietOptions.Add(new DietOption { Id = "Vegetarian" });
                //    await ctx.SaveChangesAsync();
                //}

                if (ctx.PaymentOptions.Count() < 1)
                {
                    ctx.PaymentOptions.Add(new PaymentOption { Id = "Cash" });
                    ctx.PaymentOptions.Add(new PaymentOption { Id = "Nets" });
                    ctx.PaymentOptions.Add(new PaymentOption { Id = "Visa" });
                    ctx.PaymentOptions.Add(new PaymentOption { Id = "Mastercard" });
                    ctx.PaymentOptions.Add(new PaymentOption { Id = "Diners Club International" });
                    await ctx.SaveChangesAsync();
                }
                
                if (ctx.Plans.Count() < 1)
                {
                    ctx.Plans.Add(new Plan { Id = "Plan1", Name = "Basic", Description = "Basic Plan's Description", Price = 39.99m, _Active = true, _CreatedOn = DateTime.UtcNow, StripePlanId = "prod_D2ecCI6pGvIF2g" });
                    ctx.Plans.Add(new Plan { Id = "Plan2", Name = "Intermediate", Description = "Intermediate Plan's Description", Price = 59.99m, _Active = true, _CreatedOn = DateTime.UtcNow, StripePlanId = "prod_D2eegRpvqHRd61" });
                    ctx.Plans.Add(new Plan { Id = "Plan3", Name = "Pro", Description = "Pro Plan's Description", Price = 149.99m, _Active = true, _CreatedOn = DateTime.UtcNow, StripePlanId = "prod_D2efENvd25h6ry" });
                    await ctx.SaveChangesAsync();
                }
                
                if (ctx.AzureTiers.Count() < 1)
                {
                    ctx.AzureTiers.Add(new AzureTier { Id = "F1", Pricing = "Free", AppLimit = 20 });
                    ctx.AzureTiers.Add(new AzureTier { Id = "S1", Pricing = "Standard", AppLimit = 20 });
                    ctx.AzureTiers.Add(new AzureTier { Id = "S2", Pricing = "Standard", AppLimit = 25 });

                    await ctx.SaveChangesAsync();
                }

                if (ctx.AzureResourceGroups.Count() < 1)
                {
                    AzureResourceGroup grcRg = new AzureResourceGroup { Id = "779dd8a8-7538-44ad-a176-21d9a4c00891", Location = "Southeast Asia", Name = "grc-bots-01", _Active = true, _CreatedOn = new DateTimeOffset(2018, 07, 11, 21, 00, 00, TimeSpan.FromHours(8)).UtcDateTime };
                    ctx.AzureResourceGroups.Add(grcRg);
                    
                    AzureResourceGroup customRg = new AzureResourceGroup { Id = "df8282a4-0db0-42f9-9f88-fc3a0cc7459a", Location = "Southeast Asia", Name = "custom-bots-01", _Active = true, _CreatedOn = new DateTimeOffset(2018, 07, 11, 21, 00, 00, TimeSpan.FromHours(8)).UtcDateTime };
                    customRg.AppServicePlans = new List<AzureApplicationServicePlan>();
                    customRg.AppServicePlans.Add(new AzureApplicationServicePlan { Id = "5d2fb795-f723-42aa-814d-30d39bfa4ced", Location = "Southeast Asia", Name = "custom-bots-01-f1-01", TierId = "F1", TierIndex = 1 });
                    customRg.AppServicePlans.Add(new AzureApplicationServicePlan { Id = "1f1c501e-5426-498f-aaf8-1850ed986de8", Location = "Southeast Asia", Name = "custom-bots-01-s1-01", TierId = "S1", TierIndex = 1 });
                    
                    ctx.AzureResourceGroups.Add(customRg);
                    await ctx.SaveChangesAsync();
                }
                
                if (ctx.Companies.SingleOrDefault(c => c.Name == "Nyx") == null)
                {
                    ctx.Companies.Add(new Company
                    {
                        Name = "Nyx",
                        CountryId = "Singapore",
                        State = "Singapore",
                        City = "Singapore",
                        PostalCode = "123456",                        
                        AddressLine1 = "Crescent Street",
                        AddressLine2 = "1",
                        PhoneCode = "+65",
                        PhoneNumber = "66665555",
                        
                        _Active = true,
                        _CreatedOn = DateTime.UtcNow
                    });
                    
                    await ctx.SaveChangesAsync();
                }
                
                if (ctx.CompanyAdmins.SingleOrDefault(ca => ca.Email == "admin.nyx@dispostable.com") == null)
                {
                    Company company = ctx.Companies.SingleOrDefault(c => c.Name == "Nyx");
                    CompanyAdmin ca = new CompanyAdmin
                    {
                        Email = "admin.nyx@dispostable.com",
                        UserName = "admin.nyx@dispostable.com",
                        FirstName = "John",
                        LastName = "Doe",
                        DateOfBirth = new DateTimeOffset(1990, 1, 1, 0, 0, 0, TimeSpan.FromHours(0)).DateTime,
                        ProfileImageUrl = "https://res.cloudinary.com/nyx-intelligence/nyx_wbdmhm.jpg",
                        CompanyId = company.Id,
                        EmailConfirmed = true,
                        PhoneCode = "+65",
                        PhoneNumber = "91234567",
                        
                        _FirebaseRefreshToken = "AEoYo8uKYBcE9Q4AZYPDWyMizTaUtd1uTVz_Hz77DZIqmg4a4508rvg2u4gEuSbVBEOsHIdnjwDLybnF0r73G5jrW17DP2tVNZTWYlTul6SlAydxf6XxJec0DfPV0PTcQnJ6JMM3xXB-7IhEQsZfOKqJmq-adzn6ho4Pjy8iwd0WPCMZxTCizftSHuhmAyx9lOpzDpYuO0TYvwt3Q92l0X6PRadqvF0_-g",
                        _Active = true,
                        _CreatedOn = DateTimeOffset.UtcNow
                    };
                    
                    await userManager.CreateAsync(ca, "P@ssw0rd");
                    await userManager.AddToRoleAsync(ca, "CompanyAdmin");

                    company.Admin = ca;
                    await ctx.SaveChangesAsync();
                }
            }
        }
    }
}
