﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NyxDatabase.Data;
using NyxDatabase.Models;
using NyxWebPortal.Models.Payment;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace NyxWebPortal.ViewApis
{
    [Route("local/[controller]")]
    public class PaymentController : Controller
    {
        private readonly NyxDbContext _ctx;
        
        public PaymentController(NyxDbContext ctx)
        {
            _ctx = ctx;
        }
        
        [HttpGet("[action]/{id}")]
        public async Task<IActionResult> GetEventTickets(string id)
        {
            Event eevent = await _ctx.Events
                .Include(e => e.Tickets)
                .SingleOrDefaultAsync(e => e.Id == id);

            List<ItemViewModel> itemvms = new List<ItemViewModel>();
            foreach (Item item in eevent.Tickets)
            {

                var listingSold = await _ctx.BotTransactions
                   .GroupBy(bt => bt.TransactionId)
                   .Select(g => g
                       .OrderByDescending(bt => bt._CreatedOn).First()).Where(b => b.ItemId == item.Id).ToListAsync();

                int sold = 0;

                foreach (var oneTransaction in listingSold) {
                    if (oneTransaction.Status != BotTransactionStatus.Cancelled)
                    {
                        sold += oneTransaction.Quantity;
                    }
                }

                itemvms.Add(new ItemViewModel{
                    Id = item.Id,
                    Name = item.Name,
                    Description = item.Description,
                    Price = item.Price,
                    Max = item.NumberOfItems,
                    Sold = sold
                });
            }
            
            return Ok(itemvms);
        }
    }
}
