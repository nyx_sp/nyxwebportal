﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Hangfire;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NyxDatabase.Data;
using NyxDatabase.Models;
using NyxDatabase.Models.ManyToMany;
using NyxWebPortal.Controllers;
using NyxWebPortal.Models.Workspace;
using NyxWebPortal.Services;
using NyxWebPortal.Utils;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace NyxWebPortal.ViewApis
{
    [Route("local/[controller]")]
    [Authorize(Roles = AccountController.companyAdminRole)]
    public class WorkspaceController : Controller
    {
        private readonly UserManager<NyxCustomer> _userManager;
        private readonly NyxDbContext _ctx;
        private QnAMakerService _qna;

        public WorkspaceController(
            UserManager<NyxCustomer> userManager,
            NyxDbContext ctx)
        {
            _userManager = userManager;
            _ctx = ctx;
            _qna = new QnAMakerService();
        }

        [HttpGet("[action]/{id}")]
        public async Task<IActionResult> GetBotEvents(string id, [FromQuery] int perPage, [FromQuery] int page, [FromQuery] string sortBy)
        {
            page--;
            IQueryable<Event> botEvents = _ctx.Events
                .Where(e => e.BotId == id);

            switch (sortBy)
            {
                case "name.asc":
                    botEvents = botEvents.OrderBy(e => e.Name);
                    break;
                case "name.desc":
                    botEvents = botEvents.OrderByDescending(e => e.Name);
                    break;
                case "description.asc":
                    botEvents = botEvents.OrderBy(e => e.Description);
                    break;
                case "description.desc":
                    botEvents = botEvents.OrderByDescending(e => e.Description);
                    break;
                case "hasTickets.asc":
                    botEvents = botEvents.OrderBy(e => e.HasTickets);
                    break;
                case "hasTickets.desc":
                    botEvents = botEvents.OrderByDescending(e => e.HasTickets);
                    break;
                case "status.asc":
                    botEvents = botEvents.OrderBy(e => e.Status);
                    break;
                case "status.desc":
                    botEvents = botEvents.OrderByDescending(e => e.Status);
                    break;
                case "start.asc":
                    botEvents = botEvents.OrderBy(e => e.Start);
                    break;
                case "start.desc":
                    botEvents = botEvents.OrderByDescending(e => e.Start);
                    break;
                case "end.asc":
                    botEvents = botEvents.OrderBy(e => e.End);
                    break;
                case "end.desc":
                    botEvents = botEvents.OrderByDescending(e => e.End);
                    break;
            }

            botEvents = botEvents.Skip(page * perPage)
                .Take(perPage);

            List<Event> events = await botEvents.ToListAsync();

            return Ok(events);
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> GetCustomerSupports([FromQuery] int perPage, [FromQuery] int page, [FromQuery] string sortBy)
        {
            page--;
            CompanyAdmin companyAdmin = await this.GetCompanyAdminAsync(_userManager, User);
            IQueryable<CompanyCustomerSupport> customerSupports = _ctx.CompanyCustomerSupports
                .Where(ccs => ccs.CompanyId == companyAdmin.CompanyId);

            switch (sortBy)
            {
                case "firstName.asc":
                    customerSupports = customerSupports.OrderBy(ccs => ccs.FirstName);
                    break;
                case "firstName.desc":
                    customerSupports = customerSupports.OrderByDescending(ccs => ccs.FirstName);
                    break;
                case "lastName.asc":
                    customerSupports = customerSupports.OrderBy(ccs => ccs.LastName);
                    break;
                case "lastName.desc":
                    customerSupports = customerSupports.OrderByDescending(ccs => ccs.LastName);
                    break;
                case "email.asc":
                    customerSupports = customerSupports.OrderBy(ccs => ccs.Email);
                    break;
                case "email.desc":
                    customerSupports = customerSupports.OrderByDescending(ccs => ccs.Email);
                    break;
            }

            int total = customerSupports.Count();
            customerSupports = customerSupports.Skip(page * perPage)
                .Take(perPage);

            return Ok(new
            {
                Total = total,
                CustomerSupports = await customerSupports.ToListAsync()
            });
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> GetSupportingBots([FromQuery] int perPage, [FromQuery] int page, [FromQuery] string sortBy, [FromQuery] string id = null, [FromQuery] string aid = null)
        {
            page--;
            CompanyAdmin companyAdmin = await this.GetCompanyAdminAsync(_userManager, User);
            await _ctx.Entry(companyAdmin).Reference(ca => ca.Company).LoadAsync();
            await _ctx.Entry(companyAdmin.Company).Collection(c => c.CustomerSupports).LoadAsync();

            IQueryable<Bot> bots = _ctx.Bots
                .Where(b => b.CompanyId == companyAdmin.CompanyId);

            switch (sortBy)
            {
                case "name.asc":
                    bots = bots.OrderBy(b => b.Name);
                    break;
                case "name.desc":
                    bots = bots.OrderByDescending(b => b.Name);
                    break;
                case "type.asc":
                    bots = bots.OrderBy(b => b.Type);
                    break;
                case "type.desc":
                    bots = bots.OrderByDescending(b => b.Type);
                    break;
            }

            IQueryable<SupportingBotViewModel> supportingBotVms = null;

            if (aid != null)
            {
                CompanyCustomerSupport companyCustomerSupport = await _ctx.CompanyCustomerSupports
                    .Include(ccs => ccs.SupportingBots)
                    .SingleOrDefaultAsync(ccs => ccs.Id == aid);

                supportingBotVms = bots.Select(b => new SupportingBotViewModel
                {
                    Id = b.Id,
                    Name = b.Name,
                    Type = b.Type,
                    IsSupported = companyCustomerSupport.SupportingBots.Any(bcs => bcs.BotId == b.Id)
                });
            }
            else if (id != null)
            {
                CompanyCustomerSupport companyCustomerSupport = await _ctx.CompanyCustomerSupports
                    .Include(ccs => ccs.SupportingBots)
                    .SingleOrDefaultAsync(ccs => ccs.Id == id);

                supportingBotVms = bots
                    .Where(b => companyCustomerSupport.SupportingBots.Any(bcs => bcs.BotId == b.Id))
                    .Select(b => new SupportingBotViewModel
                    {
                        Id = b.Id,
                        Name = b.Name,
                        Type = b.Type
                    });
            }

            int total = supportingBotVms.Count();
            supportingBotVms = supportingBotVms.Skip(page * perPage)
                .Take(perPage);

            return Ok(new
            {
                Total = total,
                SupportingBots = await supportingBotVms.ToListAsync()
            });
        }

        [HttpGet("[action]/{botId}")]
        public IActionResult getGeneralFeedback(string botId)
        {
            try
            {
                var generalFeedback = _ctx.FeedBacks
                    .Where(i => i.Type.Contains("general") && i.BotId == botId)
                    .Select(i => new
                    {
                        i.Id,
                        i.Name,
                        i.PhoneNumber,
                        i.Rating,
                        i.Type,
                        i.EventId,
                        i.Comment,
                        i.Email,
                    }).ToList();
                return Ok(generalFeedback);
            }
            catch (Exception e)
            {
                return BadRequest("An error has occured tryign to get General Feedback");
            }
        }

        [HttpGet("[action]/{eventId}")]
        public IActionResult getFeedback(string eventId)
        {
            try
            {
                var feedback = _ctx.FeedBacks
                    .Where(i => i.Type.Equals("feedback") && i.EventId == eventId)
                    .Select(i => new
                    {
                        i.Id,
                        i.Name,
                        i.PhoneNumber,
                        i.Rating,
                        i.EventId,
                        i.Comment,
                        i.Email,
                    }).ToList();
                return Ok(feedback);
            }
            catch (Exception e)
            {
                return BadRequest("An error has occured tryign to get General Feedback");
            }
        }

        [HttpGet("[action]/{botId}")]
        public IActionResult getVolunteer(string botId)
        {
            try
            {
                var volunteer = _ctx.FeedBacks
                    .Where(i => i.Type.Equals("volunteer") && i.BotId == botId)
                    .Select(i => new
                    {
                        i.Id,
                        i.Name,
                        i.PhoneNumber,
                        i.Rating,
                        i.EventId,
                        i.Comment,
                        i.Email,
                    }).ToList();
                return Ok(volunteer);
            }
            catch (Exception e)
            {
                return BadRequest("An error has occured tryign to get General Feedback");
            }
        }
        [HttpPost("[action]")]
        public async Task<IActionResult> setReminder(string eventId, string botUrl, string reminderDate)
        {
            try
            {
                var events = _ctx.Events.Where(i => i.Id == eventId).Single();
                DateTimeOffset reminderDateTime = DateTime.ParseExact(reminderDate, "dd/MM/yyyy h:mmtt", CultureInfo.InvariantCulture);

                events.ReminderDate = reminderDateTime;
                _ctx.Events.Update(events);
                await _ctx.SaveChangesAsync();

                // todo: scheduler
                string lol = BackgroundJob.Schedule(() => remindPeople(botUrl, eventId), reminderDateTime);
                //BackgroundJob.Delete(lol);
                return Ok("reminder has been set.");
            }
            catch (Exception e)
            {
                return BadRequest("An error has occured");
            }
        }
        [AutomaticRetry(Attempts = 2)]
        public async Task remindPeople(string botUrl, string EvntId)
        {
            HttpClient client = new HttpClient
            {
                BaseAddress = new Uri(botUrl),
            };

            JObject val = new JObject();
            val.Add("EventId", EvntId);
            string data = JsonConvert.SerializeObject(val);

            var content = new StringContent(data, Encoding.UTF8, "application/json");
            var response = await client.PostAsync("/api/SendReminder", content);

            bool status = response.IsSuccessStatusCode;

            if (!status)
            {
                throw new Exception();
            }
        }
        [AllowAnonymous]
        [HttpPost("[action]/{botId}")]
        public async Task<IActionResult> createQnA([FromBody]Newtonsoft.Json.Linq.JObject value, string botId)
        {
            try
            {
                string data = JsonConvert.SerializeObject(value);
                string kbLink = await _qna.CreateKB(data);

                var botData = _ctx.Bots.Where(i => i.Id == botId).Single();
                botData.KnowledgeBaseId = kbLink;

                _ctx.Bots.Update(botData);
                await _ctx.SaveChangesAsync();

                await _qna.PublishKB(kbLink);
                return Ok("QnA made");
            }
            catch (Exception e)
            {
                return BadRequest("An error has occured");
            }

        }

        [AllowAnonymous]
        [HttpPost("[action]/{botId}")]
        public async Task<IActionResult> updateQnA([FromBody]Newtonsoft.Json.Linq.JObject value, string botId)
        {
            var kbLink = _ctx.Bots.Where(i => i.Id == botId).Select(i => i.KnowledgeBaseId).Single();
            try
            {
                string data = JsonConvert.SerializeObject(value);
                await _qna.UpdateKB(kbLink.ToString(), data);
                await _qna.PublishKB(kbLink.ToString());

                return Ok("QnA updated");
            }
            catch (Exception e)
            {
                return BadRequest("An error has occured");
            }
        }
        [AllowAnonymous]
        [HttpGet("[action]/{botId}")]
        public async Task<IActionResult> getQnA(string botId)
        {
            try
            {
                var botData = _ctx.Bots.Where(i => i.Id == botId).Select(i => i.KnowledgeBaseId).Single();
                if (botData == null)
                {
                    return Ok(null);
                }
                var data = await _qna.GetKB(botData.ToString());
                return Ok(JsonConvert.DeserializeObject<dynamic>(data.ToString()));
            }
            catch (Exception e)
            {
                return BadRequest("An Error has occured");
            }

        }
    }
}
