﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NyxDatabase.Data;
using NyxWebPortal.Services;
using Stripe;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace NyxWebPortal.WebHooks
{
    public class StripeWebhookHandler
    {
        private readonly ILogger _log;
        private readonly IEmailSender _emailSender;

        public StripeWebhookHandler(RequestDelegate next, ILoggerFactory logger,IEmailSender emailSender)
        {
            _log = logger.CreateLogger<StripeWebhookHandler>();
            _emailSender = emailSender;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            //httpContext.Response.StatusCode = 200;
            bool handled = true;

            var json = await new StreamReader(httpContext.Request.Body).ReadToEndAsync();

            var stripeEvent = StripeEventUtility.ParseEvent(json);

            switch (stripeEvent.Type)
            {

                case StripeEvents.AccountApplicationDeauthorized:
                case StripeEvents.AccountExternalAccountCreated:
                case StripeEvents.AccountExternalAccountDeleted:
                case StripeEvents.AccountExternalAccountUpdated:
                case StripeEvents.AccountUpdated:
                    var stripeAccount = Stripe.Mapper<StripeAccount>.MapFromJson(stripeEvent.Data.Object.ToString());
                    break;
                case StripeEvents.ApplicationFeeCreated:
                case StripeEvents.ApplicationFeeRefunded:
                case StripeEvents.ApplicationFeeRefundUpdated:
                    var fee = Stripe.Mapper<StripeApplicationFee>.MapFromJson(stripeEvent.Data.Object.ToString());
                    break;
                case StripeEvents.BalanceAvailable:
                    var bal = Stripe.Mapper<StripeBalance>.MapFromJson(stripeEvent.Data.Object.ToString());
                    break;
                case StripeEvents.BitcoinReceiverCreated:
                case StripeEvents.BitcoinReceiverFilled:
                case StripeEvents.BitcoinReceiverTransactionUpdated:
                case StripeEvents.BitcoinReceiverUpdated:
                    var btcrcv = Stripe.Mapper<StripeReceiver>.MapFromJson(stripeEvent.Data.Object.ToString());
                    break;
                case StripeEvents.ChargeCaptured:
                case StripeEvents.ChargeFailed:
                case StripeEvents.ChargePending:
                case StripeEvents.ChargeRefunded:
                case StripeEvents.ChargeSucceeded:
                case StripeEvents.ChargeUpdated:
                    var charge = Stripe.Mapper<StripeCharge>.MapFromJson(stripeEvent.Data.Object.ToString());
                    break;
                case StripeEvents.ChargeDisputeClosed:
                case StripeEvents.ChargeDisputeCreated:
                case StripeEvents.ChargeDisputeFundsReinstated:
                case StripeEvents.ChargeDisputeFundsWithdrawn:
                    var dispute = Stripe.Mapper<StripeDispute>.MapFromJson(stripeEvent.Data.Object.ToString());
                    break;
                case StripeEvents.CouponCreated:
                case StripeEvents.CouponDeleted:
                case StripeEvents.CouponUpdated:
                    var cpn = Stripe.Mapper<StripeCoupon>.MapFromJson(stripeEvent.Data.Object.ToString());
                    break;
                case StripeEvents.CustomerCreated:
                case StripeEvents.CustomerDeleted:
                case StripeEvents.CustomerUpdated:
                    var cst = Stripe.Mapper<StripeCustomer>.MapFromJson(stripeEvent.Data.Object.ToString());
                    break;
                case StripeEvents.CustomerDiscountCreated:
                case StripeEvents.CustomerDiscountDeleted:
                case StripeEvents.CustomerDiscountUpdated:
                    var dsc = Stripe.Mapper<StripeDiscount>.MapFromJson(stripeEvent.Data.Object.ToString());
                    break;
                case StripeEvents.CustomerSourceCreated:
                    var src = Stripe.Mapper<StripeSource>.MapFromJson(stripeEvent.Data.Object.ToString());
                    break;
                case StripeEvents.CustomerSubscriptionCreated:
                    break;
                case StripeEvents.InvoiceCreated:
                    var invoiceCreated = Stripe.Mapper<StripeSource>.MapFromJson(stripeEvent.Data.Object.ToString());
                    await _emailSender.SendEmailAsync("tamagochi779@gmail.com", "TEST", invoiceCreated);
                    break;
                case StripeEvents.InvoicePaymentSucceeded:
                    var invoicePaymentSuccess = Stripe.Mapper<StripeSource>.MapFromJson(stripeEvent.Data.Object.ToString());
                    await _emailSender.SendEmailAsync("tamagochi779@gmail.com", "TEST", invoicePaymentSuccess);
                    break;
                case StripeEvents.InvoicePaymentFailed:
                    var invoicePaymentFailed = Stripe.Mapper<StripeSource>.MapFromJson(stripeEvent.Data.Object.ToString());
                    await _emailSender.SendEmailAsync("tamagochi779@gmail.com", "TEST", invoicePaymentFailed);
                    break;
                case StripeEvents.InvoiceItemCreated:
                    var invoiceItemCreated = Stripe.Mapper<StripeSource>.MapFromJson(stripeEvent.Data.Object.ToString());
                    await _emailSender.SendEmailAsync("tamagochi779@gmail.com", "TEST", invoiceItemCreated);
                    break;
                case StripeEvents.InvoiceItemUpdated:
                    var invoiceItemPaymentSuccess = Stripe.Mapper<StripeSource>.MapFromJson(stripeEvent.Data.Object.ToString());
                    await _emailSender.SendEmailAsync("tamagochi779@gmail.com", "TEST", invoiceItemPaymentSuccess);
                    break;
                case StripeEvents.Ping:
                    break;
                case StripeEvents.PlanCreated:
                    break;
                case StripeEvents.PayoutCreated:
                    break;
                case StripeEvents.PayoutPaid:
                    break;
                default:
                    _log.LogWarning("Received unhandled webhook: {0}", json);
                    handled = false;
                    break;
            }

            if (handled)
            {
                //WebhookEvent evt = new WebhookEvent()
                //{
                //    ServiceName = "Stripe",
                //    ServiceEventType = stripeEvent.Type.ToString(),
                //    ServiceEventId = stripeEvent.Id,
                //    Processed = DateTime.Now,
                //    EventText = json
                //};

                //await _ctx.ServiceEvents.AddAsync(evt);

                //await _ctx.SaveChangesAsync();

                httpContext.Response.StatusCode = 200;
            }
            else
                httpContext.Response.StatusCode = 400;

        }
    }

    // Extension method used to add the middleware to the HTTP request pipeline.
    public static class StripeWebhookHandlerExtensions
    {
        public static IApplicationBuilder UseStripeWebhookHandler(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<StripeWebhookHandler>();
        }
    }
}

