﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace NyxWebPortal.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Addresses",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AddressLine1 = table.Column<string>(nullable: true),
                    AddressLine2 = table.Column<string>(nullable: true),
                    BotId = table.Column<string>(nullable: true),
                    PostalCode = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Addresses", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AzureResourceGroups",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Location = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    _Active = table.Column<bool>(nullable: false),
                    _CreatedOn = table.Column<DateTimeOffset>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AzureResourceGroups", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AzureTiers",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    AppLimit = table.Column<int>(nullable: false),
                    Pricing = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AzureTiers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Countries",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Countries", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Features",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Price = table.Column<decimal>(nullable: false),
                    Type = table.Column<string>(nullable: true),
                    _Active = table.Column<bool>(nullable: false),
                    _CreatedOn = table.Column<DateTimeOffset>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Features", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PaymentOptions",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PaymentOptions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Plans",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Price = table.Column<decimal>(nullable: false),
                    StripePlanId = table.Column<string>(nullable: true),
                    Type = table.Column<string>(nullable: true),
                    _Active = table.Column<bool>(nullable: false),
                    _CreatedOn = table.Column<DateTimeOffset>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Plans", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true),
                    RoleId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AzureAppServicePlans",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Location = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    ResourceGroupId = table.Column<string>(nullable: true),
                    TierId = table.Column<string>(nullable: true),
                    TierIndex = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AzureAppServicePlans", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AzureAppServicePlans_AzureResourceGroups_ResourceGroupId",
                        column: x => x.ResourceGroupId,
                        principalTable: "AzureResourceGroups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AzureAppServicePlans_AzureTiers_TierId",
                        column: x => x.TierId,
                        principalTable: "AzureTiers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Companies",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    AddressLine1 = table.Column<string>(nullable: true),
                    AddressLine2 = table.Column<string>(nullable: true),
                    City = table.Column<string>(nullable: true),
                    CountryId = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    PhoneCode = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PostalCode = table.Column<string>(nullable: true),
                    State = table.Column<string>(nullable: true),
                    _Active = table.Column<bool>(nullable: false),
                    _CreatedOn = table.Column<DateTimeOffset>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Companies", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Companies_Countries_CountryId",
                        column: x => x.CountryId,
                        principalTable: "Countries",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    CompanyId = table.Column<string>(nullable: true),
                    CompanyId1 = table.Column<string>(nullable: true),
                    PhoneCode = table.Column<string>(nullable: true),
                    StripeConnectId = table.Column<string>(nullable: true),
                    StripeCustomerId = table.Column<string>(nullable: true),
                    CompanyCustomerSupport_CompanyId = table.Column<string>(nullable: true),
                    Id = table.Column<string>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    DateOfBirth = table.Column<DateTimeOffset>(nullable: false),
                    Discriminator = table.Column<string>(nullable: false),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEndDateUtc = table.Column<DateTimeOffset>(nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    PasswordHash = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    ProfileImageUrl = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    _Active = table.Column<bool>(nullable: false),
                    _CreatedOn = table.Column<DateTimeOffset>(nullable: false),
                    _FirebaseRefreshToken = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUsers_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AspNetUsers_Companies_CompanyId1",
                        column: x => x.CompanyId1,
                        principalTable: "Companies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AspNetUsers_Companies_CompanyCustomerSupport_CompanyId",
                        column: x => x.CompanyCustomerSupport_CompanyId,
                        principalTable: "Companies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Bots",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    AskUserUsefulMessage = table.Column<string>(nullable: true),
                    AskUserUsefulPercentage = table.Column<double>(nullable: false),
                    AzureAppServicePlanId = table.Column<string>(nullable: true),
                    AzureResourceGroupId = table.Column<string>(nullable: true),
                    CompanyId = table.Column<string>(nullable: true),
                    Discriminator = table.Column<string>(nullable: false),
                    FacebookAppId = table.Column<string>(nullable: true),
                    FacebookAppSecret = table.Column<string>(nullable: true),
                    FacebookCallbackToken = table.Column<string>(nullable: true),
                    FacebookPageAccessToken = table.Column<string>(nullable: true),
                    FacebookPageId = table.Column<string>(nullable: true),
                    FacebookVerifyToken = table.Column<string>(nullable: true),
                    KnowledgeBaseId = table.Column<string>(nullable: true),
                    MicrosoftAppId = table.Column<string>(nullable: true),
                    MicrosoftAppPassword = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    NoAnswerFoundMessage = table.Column<string>(nullable: true),
                    PlanId = table.Column<string>(nullable: true),
                    RequestContactDetailsMessage = table.Column<string>(nullable: true),
                    Status = table.Column<int>(nullable: false),
                    StripeSubscriptionId = table.Column<string>(nullable: true),
                    TelegramApiToken = table.Column<string>(nullable: true),
                    Type = table.Column<string>(nullable: true),
                    Url = table.Column<string>(nullable: true),
                    UserAgreeContactMessage = table.Column<string>(nullable: true),
                    UserDisagreeContactMessage = table.Column<string>(nullable: true),
                    UserSwearMessage = table.Column<string>(nullable: true),
                    WebChatToken = table.Column<string>(nullable: true),
                    WelcomeMessage = table.Column<string>(nullable: true),
                    WitApiKey = table.Column<string>(nullable: true),
                    _Active = table.Column<bool>(nullable: false),
                    _CreatedOn = table.Column<DateTimeOffset>(nullable: false),
                    _DeployRequestedOn = table.Column<DateTimeOffset>(nullable: false),
                    _DeployedOn = table.Column<DateTimeOffset>(nullable: false),
                    AddressId = table.Column<long>(nullable: true),
                    TeamInfoImageUrl = table.Column<string>(nullable: true),
                    VolunteerUrl = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Bots", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Bots_AzureAppServicePlans_AzureAppServicePlanId",
                        column: x => x.AzureAppServicePlanId,
                        principalTable: "AzureAppServicePlans",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Bots_AzureResourceGroups_AzureResourceGroupId",
                        column: x => x.AzureResourceGroupId,
                        principalTable: "AzureResourceGroups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Bots_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Bots_Plans_PlanId",
                        column: x => x.PlanId,
                        principalTable: "Plans",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Bots_Addresses_AddressId",
                        column: x => x.AddressId,
                        principalTable: "Addresses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: false),
                    ProviderKey = table.Column<string>(nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    RoleId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    LoginProvider = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SubscriptionPaymentLogs",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Amount = table.Column<int>(nullable: false),
                    CompanyAdminId = table.Column<string>(nullable: true),
                    Currency = table.Column<string>(nullable: true),
                    CustomerStripeId = table.Column<string>(nullable: true),
                    Status = table.Column<int>(nullable: false),
                    TransactionId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubscriptionPaymentLogs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SubscriptionPaymentLogs_AspNetUsers_CompanyAdminId",
                        column: x => x.CompanyAdminId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Annoucement",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    AnnoucementDateTime = table.Column<DateTimeOffset>(nullable: false),
                    BotId = table.Column<string>(nullable: true),
                    GRCBotId = table.Column<string>(nullable: true),
                    SenderEmail = table.Column<string>(nullable: true),
                    SenderId = table.Column<string>(nullable: true),
                    SenderName = table.Column<string>(nullable: true),
                    Text = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Annoucement", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Annoucement_Bots_BotId",
                        column: x => x.BotId,
                        principalTable: "Bots",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Annoucement_Bots_GRCBotId",
                        column: x => x.GRCBotId,
                        principalTable: "Bots",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "BotCustomerSupports",
                columns: table => new
                {
                    BotId = table.Column<string>(nullable: false),
                    CustomerSupportId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BotCustomerSupports", x => new { x.BotId, x.CustomerSupportId });
                    table.ForeignKey(
                        name: "FK_BotCustomerSupports_Bots_BotId",
                        column: x => x.BotId,
                        principalTable: "Bots",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BotCustomerSupports_AspNetUsers_CustomerSupportId",
                        column: x => x.CustomerSupportId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BotShippingDestination",
                columns: table => new
                {
                    BotId = table.Column<string>(nullable: false),
                    CountryId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BotShippingDestination", x => new { x.BotId, x.CountryId });
                    table.ForeignKey(
                        name: "FK_BotShippingDestination_Bots_BotId",
                        column: x => x.BotId,
                        principalTable: "Bots",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BotTransactions",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    AdminId = table.Column<string>(nullable: true),
                    AdminName = table.Column<string>(nullable: true),
                    Amount = table.Column<decimal>(nullable: false),
                    BotId = table.Column<string>(nullable: true),
                    BotUserId = table.Column<string>(nullable: true),
                    CompanyId = table.Column<string>(nullable: true),
                    CompanyName = table.Column<string>(nullable: true),
                    ItemId = table.Column<string>(nullable: true),
                    ItemName = table.Column<string>(nullable: true),
                    Quantity = table.Column<int>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    StripeReceiptId = table.Column<string>(nullable: true),
                    TransactionId = table.Column<string>(nullable: true),
                    _CreatedOn = table.Column<DateTimeOffset>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BotTransactions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BotTransactions_Bots_BotId",
                        column: x => x.BotId,
                        principalTable: "Bots",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Events",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    BotId = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    End = table.Column<DateTimeOffset>(nullable: false),
                    FeedbackDate = table.Column<DateTimeOffset>(nullable: false),
                    GRCBotId = table.Column<string>(nullable: true),
                    HasTickets = table.Column<bool>(nullable: false),
                    ImageUrl = table.Column<string>(nullable: true),
                    Location = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    ReminderDate = table.Column<DateTimeOffset>(nullable: true),
                    Start = table.Column<DateTimeOffset>(nullable: false),
                    Status = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Events", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Events_Bots_BotId",
                        column: x => x.BotId,
                        principalTable: "Bots",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Events_Bots_GRCBotId",
                        column: x => x.GRCBotId,
                        principalTable: "Bots",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "FeatureAddOns",
                columns: table => new
                {
                    BotId = table.Column<string>(nullable: false),
                    FeatureId = table.Column<string>(nullable: false),
                    SubscriptionId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FeatureAddOns", x => new { x.BotId, x.FeatureId });
                    table.ForeignKey(
                        name: "FK_FeatureAddOns_Bots_BotId",
                        column: x => x.BotId,
                        principalTable: "Bots",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_FeatureAddOns_Features_FeatureId",
                        column: x => x.FeatureId,
                        principalTable: "Features",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FeedBacks",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    BotId = table.Column<string>(nullable: true),
                    Comment = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    EventId = table.Column<string>(nullable: true),
                    GRCBotId = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    Rating = table.Column<int>(nullable: false),
                    Type = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FeedBacks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FeedBacks_Bots_BotId",
                        column: x => x.BotId,
                        principalTable: "Bots",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_FeedBacks_Events_EventId",
                        column: x => x.EventId,
                        principalTable: "Events",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_FeedBacks_Bots_GRCBotId",
                        column: x => x.GRCBotId,
                        principalTable: "Bots",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Items",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    EndDate = table.Column<DateTimeOffset>(nullable: false),
                    EventId = table.Column<string>(nullable: true),
                    IsReservable = table.Column<bool>(nullable: false),
                    MaxReservablePerOrder = table.Column<int>(nullable: false),
                    MaxReserveHoldDays = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    NumberOfItems = table.Column<int>(nullable: false),
                    Price = table.Column<decimal>(nullable: false),
                    StartDate = table.Column<DateTimeOffset>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Items", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Items_Events_EventId",
                        column: x => x.EventId,
                        principalTable: "Events",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Annoucement_BotId",
                table: "Annoucement",
                column: "BotId");

            migrationBuilder.CreateIndex(
                name: "IX_Annoucement_GRCBotId",
                table: "Annoucement",
                column: "GRCBotId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_CompanyId",
                table: "AspNetUsers",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_CompanyId1",
                table: "AspNetUsers",
                column: "CompanyId1",
                unique: true,
                filter: "[CompanyId1] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_CompanyCustomerSupport_CompanyId",
                table: "AspNetUsers",
                column: "CompanyCustomerSupport_CompanyId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AzureAppServicePlans_ResourceGroupId",
                table: "AzureAppServicePlans",
                column: "ResourceGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_AzureAppServicePlans_TierId",
                table: "AzureAppServicePlans",
                column: "TierId");

            migrationBuilder.CreateIndex(
                name: "IX_BotCustomerSupports_CustomerSupportId",
                table: "BotCustomerSupports",
                column: "CustomerSupportId");

            migrationBuilder.CreateIndex(
                name: "IX_Bots_AzureAppServicePlanId",
                table: "Bots",
                column: "AzureAppServicePlanId");

            migrationBuilder.CreateIndex(
                name: "IX_Bots_AzureResourceGroupId",
                table: "Bots",
                column: "AzureResourceGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_Bots_CompanyId",
                table: "Bots",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_Bots_PlanId",
                table: "Bots",
                column: "PlanId");

            migrationBuilder.CreateIndex(
                name: "IX_Bots_AddressId",
                table: "Bots",
                column: "AddressId");

            migrationBuilder.CreateIndex(
                name: "IX_BotTransactions_BotId",
                table: "BotTransactions",
                column: "BotId");

            migrationBuilder.CreateIndex(
                name: "IX_Companies_CountryId",
                table: "Companies",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_Events_BotId",
                table: "Events",
                column: "BotId");

            migrationBuilder.CreateIndex(
                name: "IX_Events_GRCBotId",
                table: "Events",
                column: "GRCBotId");

            migrationBuilder.CreateIndex(
                name: "IX_FeatureAddOns_FeatureId",
                table: "FeatureAddOns",
                column: "FeatureId");

            migrationBuilder.CreateIndex(
                name: "IX_FeedBacks_BotId",
                table: "FeedBacks",
                column: "BotId");

            migrationBuilder.CreateIndex(
                name: "IX_FeedBacks_EventId",
                table: "FeedBacks",
                column: "EventId");

            migrationBuilder.CreateIndex(
                name: "IX_FeedBacks_GRCBotId",
                table: "FeedBacks",
                column: "GRCBotId");

            migrationBuilder.CreateIndex(
                name: "IX_Items_EventId",
                table: "Items",
                column: "EventId");

            migrationBuilder.CreateIndex(
                name: "IX_SubscriptionPaymentLogs_CompanyAdminId",
                table: "SubscriptionPaymentLogs",
                column: "CompanyAdminId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Annoucement");

            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "BotCustomerSupports");

            migrationBuilder.DropTable(
                name: "BotShippingDestination");

            migrationBuilder.DropTable(
                name: "BotTransactions");

            migrationBuilder.DropTable(
                name: "FeatureAddOns");

            migrationBuilder.DropTable(
                name: "FeedBacks");

            migrationBuilder.DropTable(
                name: "Items");

            migrationBuilder.DropTable(
                name: "PaymentOptions");

            migrationBuilder.DropTable(
                name: "SubscriptionPaymentLogs");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "Features");

            migrationBuilder.DropTable(
                name: "Events");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "Bots");

            migrationBuilder.DropTable(
                name: "AzureAppServicePlans");

            migrationBuilder.DropTable(
                name: "Companies");

            migrationBuilder.DropTable(
                name: "Plans");

            migrationBuilder.DropTable(
                name: "Addresses");

            migrationBuilder.DropTable(
                name: "AzureResourceGroups");

            migrationBuilder.DropTable(
                name: "AzureTiers");

            migrationBuilder.DropTable(
                name: "Countries");
        }
    }
}
