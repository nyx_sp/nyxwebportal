﻿using System;
using System.ComponentModel.DataAnnotations;

namespace NyxWebPortal.Models.Accounts
{
    public class ResetPasswordViewModel
    {
        [Required]
        public string Id { get; set; }
        
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        
        [Required]
        public string NewPassword { get; set; }
        
        [Required]
        public string ConfirmPassword { get; set; }
        
        [Required]
        public string Code { get; set; }
    }
}
