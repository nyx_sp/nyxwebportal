﻿using Microsoft.AspNetCore.Http;
using System;
using System.ComponentModel.DataAnnotations;

namespace NyxWebPortal.Models.Accounts
{
    public class EditProfileViewModel
    {
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string DOB { get; set; }
        [Required]
        public string PhoneCode { get; set; }
        [Required]
        public string PhoneNumber { get; set; }
        public string ImgUrl { get; set; }
        public IFormFile Image { get; set; }
    }
}
