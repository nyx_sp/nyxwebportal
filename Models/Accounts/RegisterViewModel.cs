﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace NyxWebPortal.Models.Accounts
{
    public class RegisterViewModel
    {
        [Required]
        [EmailAddress]
        public string AdminEmail { get; set; }
        
        [Required]
        public string AdminFirstName { get; set; }
        
        [Required]
        public string AdminLastName { get; set; }
        
        public string AdminDOB { get; set; }
        
        [Required]
        public string AdminPhoneCode { get; set; }
        
        [Required]
        public string AdminPhoneNumber { get; set; }
        
        public IFormFile AdminProfileImg { get; set; }
        
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }
        
        [Required]
        public string CompanyName { get; set; }
        
        [Required]
        public string CompanyCountry { get; set; }
        
        [Required]
        public string CompanyState { get; set; }
        
        [Required]
        public string CompanyCity { get; set; }

        [Required]
        public string CompanyAddressLine1 { get; set; }
        
        [Required]
        public string CompanyAddressLine2 { get; set; }
        
        [Required]
        public string CompanyPostalCode { get; set; }
        
        [Required]
        public string CompanyPhoneCode { get; set; }
        
        [Required]
        public string CompanyPhoneNumber { get; set; }

        public string StripeTransactionId { get; set; }
    }
}
