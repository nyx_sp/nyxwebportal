﻿using System;
using Microsoft.AspNetCore.Http;

namespace NyxWebPortal.Models.Accounts
{
    public class ChangeProfileImageViewModel
    {
        public IFormFile Image { get; set; }
    }
}
