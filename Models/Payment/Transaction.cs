﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NyxWebPortal.Models.Payment
{
    public class TransactionViewModel
    {
        public string ItemId { get; set; }
        public int Quantity { get; set; }
    }
}
