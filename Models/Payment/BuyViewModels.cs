﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;

namespace NyxWebPortal.Models.Payment
{
    // You may need to install the Microsoft.AspNetCore.Http.Abstractions package into your project
    public class BuyEventTicketsViewModel
    {
        public string EventId { get; set; }
        public string BotId { get; set; }
        public string StripeTransactionId { get; set; }
        public string Email { get; set; }

        public List<ItemViewModel> Items { get; set; }
    }
}
