﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NyxDatabase.Models;

namespace NyxWebPortal.Models.Payment
{
    public class Pay
    {
        public string TransactionId { get; set; }
    }
    
    public class PayViewModel
    {
        public string BotId { get; set; }
        public string BotName { get; set; }
        public string CompanyName { get; set; }
        public string EventId { get; set; }
        public string BotUserId { get; set; }
    }
    
    public class ItemViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Quantity { get; set; }
        public int Sold { get; set; }
        public int Max { get; set; }
        public decimal Price { get; set; }
    }
}
