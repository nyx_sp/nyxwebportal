﻿using System;
namespace NyxWebPortal.Models.Workspace
{
    public class SupportingBotViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Url { get; set; }
        public string PlanId { get; set; }
        public bool IsSupported { get; set; }
    }
}
