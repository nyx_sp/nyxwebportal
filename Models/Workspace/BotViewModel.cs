﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using NyxDatabase.Models;

namespace NyxWebPortal.Models.Workspace
{
    public class BotViewModel
    {
        public string Id { get; set; }
        public List<Plan> Plans { get; set; } = new List<Plan>();
        public List<string> Countries { get; set; } = new List<string>();
        public string Name { get; set; }
        public string Type { get; set; }
        public string Url { get; set; }
        public string WelcomeMessage { get; set; }
        public string NoAnswerFoundMessage { get; set; }
        public string RequestContactDetailsMessage { get; set; }
        public string UserAgreeContactMessage { get; set; }
        public string UserDisagreeContactMessage { get; set; }
        public string UserSwearMessage { get; set; }
        public string AskUserUsefulMessage { get; set; }
        public double AskUserUsefulPercentage { get; set; }
        public string PlanId { get; set; }
        public string StripePlanId { get; set; }
        public string StripeCustomerId { get; set; }
        public string StripeSubscription { get; set; }


    }

    public class GrcBotViewModel : BotViewModel
    {
        public IFormFile TeamImage { get; set; }
        public string VolunteerUrl { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string PostalCode { get; set; }
        
        // to show
        public string TeamImageUrl { get; set; }
    }
    
    public class ShopBotViewModel : BotViewModel
    {
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string PostalCode { get; set; }
        public bool HasExchange { get; set; }
        public bool NeedExchangeReceipt { get; set; }
        public int ExchangeWithin { get; set; }
        public bool HasDelivery { get; set; }
        public bool HasShipping { get; set; }
        public string Mailing { get; set; }
        public string Website { get; set; }
        public List<string> ShippingCountries { get; set; } = new List<string>();
    }
}
