﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace NyxWebPortal.Models.Workspace
{
    // You may need to install the Microsoft.AspNetCore.Http.Abstractions package into your project
    public class EventViewModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Location { get; set; }
        public bool HasTickets { get; set; }
        public List<EventItemViewModel> EventItems { get; set; }
        public string StartDateTime { get; set; }
        public string EndDateTime { get; set; }
        public string FeedbackDateTime { get; set; }
        public string BotId { get; set; }
        public string EventStatus { get; set; }
        public IFormFile EventImage { get; set; }

        
    }

    public class EventItemViewModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int? NumberOfTickets { get; set; }
        public decimal? TicketPrice { get; set; }
        public string StartDateTime { get; set; }
        public string EndDateTime { get; set; }
        public int MaxReserveHoldDays { get; set; }
        public int MaxReservablePerOrder { get; set; }
        public bool IsReservable { get; set; }
    }

    public class ItemViewModel
    {
        public string EventId { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int NumberOfItems { get; set; }
        public decimal Price { get; set; }
        public DateTimeOffset StartDate { get; set; }
        public DateTimeOffset EndDate { get; set; }
        public int MaxReserveHoldDays { get; set; }
        public int MaxReservablePerOrder { get; set; }
        public bool IsReservable { get; set; }
        public int TicketsSold { get; set; }
    }
}
