﻿using System;
namespace NyxWebPortal.Models.Workspace
{
    public class PublishBotViewModel
    {
        public string TelegramApiToken { get; set; }
        public string FacebookPageId { get; set; }
        public string FacebookAppId { get; set; }
        public string FacebookAppSecret { get; set; }
        public string FacebookPageAccessToken { get; set; }
    }
}
